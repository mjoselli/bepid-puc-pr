//
//  NameViewController.h
//  RPG
//
//  Created by Mark Joselli on 3/12/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NameViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *GoButton;

@end
