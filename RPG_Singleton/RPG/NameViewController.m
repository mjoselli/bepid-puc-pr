//
//  NameViewController.m
//  RPG
//
//  Created by Mark Joselli on 3/12/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "NameViewController.h"
#import "DataManager.h"


@interface NameViewController (){
    NSString *nome;
}

@end

@implementation NameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    nome = @"";
    _GoButton.hidden = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    nome = textField.text;
    [textField resignFirstResponder];
    
    if(nome.length > 0)
        _GoButton.hidden = NO;
    
    [[[DataManager sharedManager] dataDict] setValue:nome forKey:@"nome"];
    
    return YES;
    
}


@end
