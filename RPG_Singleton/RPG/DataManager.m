//
//  DataManager.m
//  RPG
//
//  Created by Mark Joselli on 3/12/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

+ (id)sharedManager {
    static DataManager *sharedDataManager = nil;
   @synchronized(self) {
       if (sharedDataManager == nil){
           sharedDataManager = [[self alloc] init];
        }
   }
    return sharedDataManager;
}

- (id)init {
    if (self = [super init]) {
        _dataDict = [[NSMutableDictionary alloc]init];
    }
    return self;
}



@end
