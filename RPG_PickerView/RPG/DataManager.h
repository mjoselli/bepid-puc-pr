//
//  DataManager.h
//  RPG
//
//  Created by Mark Joselli on 3/12/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject


@property NSMutableDictionary *dataDict;
    

+ (id)sharedManager;



@end
