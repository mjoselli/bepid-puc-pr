//
//  NameViewController.m
//  RPG
//
//  Created by Mark Joselli on 3/12/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "NameViewController.h"
#import "DataManager.h"


@interface NameViewController (){
    NSString *nome;
}

@end

@implementation NameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    nome = @"";
    _GoButton.hidden = YES;
    
    pickerData = @[@"Facil",@"Normal",@"Difícil"];
    
    dificulty = 0;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    dificulty = (int)row;
    return pickerData[row];
}





- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    nome = textField.text;
    [textField resignFirstResponder];
    
    if(nome.length > 0)
        _GoButton.hidden = NO;
    
    [[[DataManager sharedManager] dataDict] setValue:nome forKey:@"nome"];
    
    [[[DataManager sharedManager] dataDict] setValue:pickerData[dificulty] forKey:@"dificuldade"];
    
    return YES;
    
}


@end
