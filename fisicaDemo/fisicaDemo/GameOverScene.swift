//
//  GameOverScene.swift
//  testeSpriteKit
//
//  Created by Mark Joselli on 5/19/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

import SpriteKit

class GameOverScene: SKScene {
    
    var ganhou = true
    override func didMoveToView(view: SKView) {
        
        let myLabel = SKLabelNode(fontNamed:"Chalkduster")
        
        if ganhou {
            myLabel.text = "Vc Ganhou!"
        }else{
            myLabel.text = "Vc Perdeu!"
        }
        
        myLabel.fontSize = 65
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        
        self.addChild(myLabel)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        let scene = GameScene(size: CGSizeMake(1334,750))
        
        scene.scaleMode = self.scaleMode
        
        view?.presentScene(scene)
    }
    
    
}
