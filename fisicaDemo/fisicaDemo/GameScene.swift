//
//  GameScene.swift
//  fisicaDemo
//
//  Created by Mark Joselli on 5/26/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

import SpriteKit

class GameScene: SKScene,SKPhysicsContactDelegate {
    
    let hero = SKLabelNode(fontNamed: "Apple Color Emoji")
    let coco = SKLabelNode(fontNamed: "Apple Color Emoji")
    let fogo = SKLabelNode(fontNamed: "Apple Color Emoji")
    
    var feedbackItem = SKShapeNode(circleOfRadius: 10.0)
    
    let cocoCategory: UInt32 = 0x1 << 0
    let fogoCategory: UInt32 = 0x1 << 1
    
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        //self.size = view.bounds.size
        self.physicsWorld.contactDelegate = self
        let edge = SKShapeNode()
        println(self.frame)
        let edgePath = CGPathCreateWithRect(self.frame, nil)
        edge.path = edgePath
        edge.strokeColor = UIColor.yellowColor()
        edge.lineWidth = 1.0
        edge.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        
        self.addChild(edge)
        
        var floor = SKSpriteNode(color: SKColor.brownColor(), size: CGSizeMake(CGRectGetWidth(self.frame), 20))
        
        floor.anchorPoint = CGPointZero
        floor.position = CGPointZero
        floor.physicsBody = SKPhysicsBody(edgeLoopFromRect: floor.frame)
        floor.physicsBody?.dynamic = false
        floor.physicsBody?.friction = 1
        
        addChild(floor)
        
        
        var sling = SKSpriteNode(color: SKColor.brownColor(), size: CGSizeMake(20, 150))
        
        sling.anchorPoint = CGPointZero
        sling.position = CGPoint(
            x: 140,
            y: 0)
        
        sling.physicsBody?.dynamic = false
        
        addChild(sling)
    
        hero.fontSize = 48
        hero.text = "😀"
        hero.position = CGPoint(
            x: 150,
            y: 150)
        
        hero.physicsBody = SKPhysicsBody(circleOfRadius: hero.frame.width/2 * 0.8, center: CGPointMake(0,hero.frame.height/2 * 0.8 ))
            
            //circleOfRadius: item.frame.width/2 )
        hero.physicsBody?.dynamic = false
        hero.physicsBody?.allowsRotation = false
        //hero.physicsBody?.restitution = 0.5
        hero.physicsBody?.friction = 1.0
        
        feedbackItem.fillColor = SKColor.redColor()
        
        
        self.addChild(hero)
        
        backgroundColor = SKColor.whiteColor()
        
        
        for i in 1...12 {
            self.addChild(createBox(CGPointMake(CGFloat(1200), CGFloat(51 * i))))
        }
        
        
        
        coco.fontSize = 48
        coco.text = "💩"
        coco.position = CGPoint(
            x: 1200,
            y: 12*51)
        
        coco.physicsBody = SKPhysicsBody(rectangleOfSize: coco.frame.size, center: CGPointMake(0,coco.frame.height/2 * 0.8 ))
        
        
        coco.physicsBody?.categoryBitMask = cocoCategory
        coco.physicsBody?.contactTestBitMask = fogoCategory

        //circleOfRadius: item.frame.width/2 )
        coco.physicsBody?.dynamic = true
        addChild(coco)
        
        
        fogo.text = "🔥"
        fogo.position = CGPoint(
            x: 1000,
            y: 20)
        
        fogo.physicsBody = SKPhysicsBody(rectangleOfSize: coco.frame.size, center: CGPointMake(0,coco.frame.height/2 * 0.8 ))
        
        
        fogo.physicsBody?.categoryBitMask = fogoCategory
        fogo.physicsBody?.contactTestBitMask = cocoCategory
        
        //circleOfRadius: item.frame.width/2 )
        fogo.physicsBody?.dynamic = false
        addChild(fogo)

        
        
        
        
    }
    
    func createBox(location: CGPoint) -> SKSpriteNode{
        var box = SKSpriteNode(color: SKColor.grayColor(), size: CGSizeMake(50, 50))
        
        box.position = location
        box.physicsBody = SKPhysicsBody(rectangleOfSize: box.size)
        box.physicsBody?.dynamic = true
        
        return box
        
    }
    
    
    var beginLocation = CGPointZero
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        if let touch = touches.first as? UITouch {
            beginLocation = touch.locationInNode(self)
            
           feedbackItem.position = beginLocation
            addChild(feedbackItem)
            
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var location = touch.locationInNode(self)
            feedbackItem.position = location
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var endLocation = touch.locationInNode(self)
            var dY = beginLocation.y - endLocation.y
            var dX = beginLocation.x - endLocation.x
            
            hero.physicsBody?.dynamic = true
            hero.physicsBody?.applyForce(CGVectorMake(50*dX, 50*dY))
            feedbackItem.removeFromParent()
            
            
            
        }
        
            
            
            
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        let collision = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        if collision == cocoCategory | fogoCategory {
            contact.bodyA.node!.removeFromParent()
            contact.bodyB.node!.removeFromParent()
            
            let gameOverScene = GameOverScene(size: size)
            gameOverScene.scaleMode = scaleMode
            gameOverScene.ganhou = true
            let reveal = SKTransition.flipHorizontalWithDuration(0.5)
            view?.presentScene(gameOverScene, transition: reveal)
        }
        
    }
    
    
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        if hero.physicsBody!.dynamic && hero.physicsBody!.resting{
            self.runAction(SKAction.waitForDuration(0.3), completion: { () -> Void in
                let gameOverScene = GameOverScene(size: self.size)
                gameOverScene.scaleMode = self.scaleMode
                gameOverScene.ganhou = false
                let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                self.view?.presentScene(gameOverScene, transition: reveal)
                })
        }
        
    }
}
