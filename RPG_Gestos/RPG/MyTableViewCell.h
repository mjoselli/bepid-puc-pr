//
//  MyTableViewCell.h
//  RPG
//
//  Created by Mark Joselli on 3/19/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageArma;
@property (weak, nonatomic) IBOutlet UILabel *labelArma;
@property (weak, nonatomic) IBOutlet UITextView *textViewArma;


@end
