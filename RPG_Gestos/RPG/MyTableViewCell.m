//
//  MyTableViewCell.m
//  RPG
//
//  Created by Mark Joselli on 3/19/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "MyTableViewCell.h"

@implementation MyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
