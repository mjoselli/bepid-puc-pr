//
//  NameViewController.m
//  RPG
//
//  Created by Mark Joselli on 3/12/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "NameViewController.h"
#import "DataManager.h"


@interface NameViewController (){
    NSString *nome;
}

@end

@implementation NameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    nome = @"";
    
    pickerData = @[@"Facil",@"Normal",@"Difícil"];
    
    dificulty = 0;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    dificulty = (int)row;
    return pickerData[row];
}





- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    
    
    return YES;
    
}


- (IBAction)GoButtonPressed:(id)sender {
    nome = _NameTextField.text;
    
    if(nome.length < 1){
        UIAlertController *alertController = [UIAlertController
                                    alertControllerWithTitle:@"Erro"
                                              message:@"Favor inserir um nome!"
                                              preferredStyle:UIAlertControllerStyleAlert
                                              ];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:Nil];

        return;
    }
    
    [[[DataManager sharedManager] dataDict] setValue:nome forKey:@"nome"];
    
    [[[DataManager sharedManager] dataDict] setValue:pickerData[dificulty] forKey:@"dificuldade"];
    
    [self performSegueWithIdentifier:@"showPersonaguem" sender:self];
}
@end
