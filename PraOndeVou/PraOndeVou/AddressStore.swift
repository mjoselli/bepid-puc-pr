//
//  AddressStore.swift
//  PraOndeVou
//
//  Created by Maicris Fernandes on 06/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import Foundation

class AddressStore {
  
  private var _addressArray = [Address]()
  
  class var sharedInstance: AddressStore {
    struct Static {
      static let instance: AddressStore = AddressStore()
    }
    return Static.instance
  }
  
  init() {
    self.populate()
  }
  
  private func populate() {
    let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
    let filePath = "\(paths[0])/data.txt"
    
    if NSFileManager.defaultManager().fileExistsAtPath(filePath) {
      let file = String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding, error: nil)!
      let registros = file.componentsSeparatedByString("|")
      if registros.count > 0 {
        for registro in registros {
          let addressArray = registro.componentsSeparatedByString(";")
          var address: Address = Address()
          address.name = addressArray[0]
          address.street = addressArray[1]
          address.city = addressArray[2]
          address.state = addressArray[3]
          _addressArray += [address]
        }
      }
    }
  }
  
  private func save() {
    let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
    let filePath = "\(paths[0])/data.txt"
    
    var data: String = ""
    for var i = 0; i < _addressArray.count; i++ {
      let address = _addressArray[i]
      if i > 0 {
        data += "|"
      }
      data += "\(address.name);\(address.street);\(address.city);\(address.state)"
    }
    data.writeToFile(filePath, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
  }
  
  //Métodos Públicos
  func newAddress(address: Address) {
    _addressArray += [address]
    self.save()
  }
  
  func getAddressWithIndex(index: Int) -> Address {
    return _addressArray[index]
  }
  
  func count() -> Int {
    return _addressArray.count
  }
}