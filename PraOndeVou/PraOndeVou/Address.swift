//
//  Address.swift
//  PraOndeVou
//
//  Created by Maicris Fernandes on 06/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import Foundation

class Address {
  var name: String = ""
  var street: String = ""
  var city: String = ""
  var state: String = ""
  var zipCode: String = ""
  var country: String = ""
  
  func geoCodeAddress() -> String {
    return ("\(street), \(city), \(state), \(country)")
  }
  
  func description() -> String {
    return "Endereço: \(street)\nCidade: \(city)\nEstado: \(state)\nPaís: \(country)\nCEP: \(zipCode)"
  }
}