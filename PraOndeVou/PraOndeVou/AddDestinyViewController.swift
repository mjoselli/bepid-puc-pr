//
//  AddDestinyViewController.swift
//  PraOndeVou
//
//  Created by Maicris Fernandes on 07/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import AddressBook

class AddDestinyViewController: UIViewController, UITextFieldDelegate, MKMapViewDelegate {
  
  var currentLocation: CLLocation?
  
  private let addressStore: AddressStore = AddressStore.sharedInstance
  private let address: Address = Address()
  private var currentTextField: UITextField?
  
  @IBOutlet weak var txtName: UITextField!
  @IBOutlet weak var txtStreet: UITextField!
  @IBOutlet weak var txtCity: UITextField!
  @IBOutlet weak var txtState: UITextField!
  @IBOutlet weak var map: MKMapView!
  @IBOutlet weak var btnAdd: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    map.showsUserLocation = true
    self.setUserLocation()
  }
  
  func setUserLocation() {
    if let location = currentLocation {
      let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000)
      map.setRegion(region, animated: false)
    }
  }
  
  @IBAction func back(sender: AnyObject) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func add(sender: AnyObject) {
    addressStore.newAddress(address)
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func loadOnMap(sender: AnyObject) {
    let geocoder = CLGeocoder()
    
    if let textField = currentTextField {
      textField.resignFirstResponder()
    }
    
    address.name = txtName.text
    address.street = txtStreet.text
    address.city = txtCity.text
    address.state = txtState.text
    btnAdd.enabled = false
    
    geocoder.geocodeAddressString(address.geoCodeAddress(), completionHandler: {(placemarks: [AnyObject]!, error: NSError!) in
      if error != nil {
        println("Falha de execução: \(error.localizedDescription)")
        return
      }
      if placemarks.count > 0 {
        let placemark = placemarks[0] as! CLPlacemark
        self.btnAdd.enabled = true
        self.map.removeAnnotations(self.map.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.location.coordinate
        annotation.title = self.address.name
        self.map.addAnnotation(annotation)
        let region = MKCoordinateRegionMakeWithDistance(placemark.location.coordinate, 1000, 1000)
        self.map.setRegion(region, animated: false)
      }
    })
  }
  
  @IBAction func changeType(sender: AnyObject) {
    if map.mapType == .Standard {
      map.mapType = .Satellite
    } else {
      map.mapType = .Standard
    }
  }
  
  @IBAction func clean(sender: AnyObject) {
    map.removeAnnotations(map.annotations)
    self.setUserLocation()
    txtName.text = ""
    txtStreet.text = ""
    txtCity.text = ""
    txtState.text = ""
    btnAdd.enabled = false
  }
  
  //UITextFieldDelegate
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    currentTextField = textField
    
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    
    return true
  }
  
  //MKMapViewDelegate
  func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
    let annotationIdentifier = "annotationIdentifier"
    var annotationView = map.dequeueReusableAnnotationViewWithIdentifier(annotationIdentifier)
    
    if annotation.isKindOfClass(MKUserLocation) {
      return annotationView
    }
    if annotationView == nil {
      annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
      
      let bobImage = UIImage(named: "bob.png")
      annotationView.image = bobImage
      annotationView.canShowCallout = true
    } else {
      annotationView.annotation = annotation
    }
    
    return annotationView
  }
}