//
//  ViewController.swift
//  PraOndeVou
//
//  Created by Maicris Fernandes on 06/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import AddressBook

class ViewController: UIViewController, CLLocationManagerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

  private let addressStore: AddressStore = AddressStore.sharedInstance
  private let locationManager: CLLocationManager = CLLocationManager()
  private var currentLocation: CLLocation?
  private let geocoder: CLGeocoder = CLGeocoder()
  private var actualDestiny = -1
  
  @IBOutlet weak var txtMyLocation: UITextView!
  @IBOutlet weak var picLocations: UIPickerView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initCurrentLocation()
  }
  
  override func viewWillAppear(animated: Bool) {
    picLocations.reloadAllComponents()
    if picLocations.numberOfRowsInComponent(0) > 0 {
      actualDestiny = 0
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "AddLocation" {
      let advc = segue.destinationViewController as! AddDestinyViewController
      advc.currentLocation = currentLocation
    }
  }
  
  //Inicializações
  func initCurrentLocation() {
    locationManager.delegate = self
    locationManager.distanceFilter = 100
    if (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 8.0 {
      let codeStatus = CLLocationManager.authorizationStatus()
      locationManager.requestWhenInUseAuthorization()
    }
    locationManager.startUpdatingLocation()
  }
  
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    currentLocation = (locations[0] as! CLLocation)
    
    geocoder.reverseGeocodeLocation(currentLocation, completionHandler: {(placemarks: [AnyObject]!, error: NSError!) in
      if error != nil {
        println("Falha de execução: \(error.localizedDescription)")
        return
      }
      if placemarks.count > 0 {
        let placemark = placemarks[0] as! CLPlacemark
        let addressDict = placemark.addressDictionary
        let address: Address = Address()
        
        address.street = addressDict[kABPersonAddressStreetKey] as! String
        address.city = addressDict[kABPersonAddressCityKey] as! String
        address.state = addressDict[kABPersonAddressStateKey] as! String
        address.country = addressDict[kABPersonAddressCountryKey] as! String
        address.zipCode = addressDict[kABPersonAddressZIPKey] as! String
        
        self.txtMyLocation.text = address.description()
      }
    })
  }
  
  //UIPickerViewDataSource
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return addressStore.count()
  }
  
  //UIPIckerViewDelegate
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
    let address: Address = addressStore.getAddressWithIndex(row)
    
    return address.name
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    actualDestiny = row
  }
  
  //Actions
  @IBAction func goMap(sender: AnyObject) {
    if actualDestiny == -1 {
      return
    }
    
    let address: Address = addressStore.getAddressWithIndex(actualDestiny)
    
    geocoder.geocodeAddressString(address.geoCodeAddress(), completionHandler: {(placemarks: [AnyObject]!, error: NSError!) in
      if error != nil {
        println("Falha de execução: \(error.localizedDescription)")
        return
      }
      if placemarks.count > 0 {
        let placemark = placemarks[0] as! CLPlacemark
        self.showMapWithLocation(placemark)
      }
    })
  }
  
  //Métodos Privados
  private func showMapWithLocation(placemark: CLPlacemark) {
    let mkPlacemark = MKPlacemark(placemark: placemark)
    let mapItem = MKMapItem(placemark: mkPlacemark)
    let mapItemCurrent = MKMapItem.mapItemForCurrentLocation()
    let mapItems = [mapItemCurrent, mapItem]
    let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
    
    MKMapItem.openMapsWithItems(mapItems, launchOptions: options)
  }
}

