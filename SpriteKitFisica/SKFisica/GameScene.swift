//
//  GameScene.swift
//  SKFisica
//
//  Created by Maicris Fernandes on 18/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    private var menu01: SKLabelNode!
    private var menu02: SKLabelNode!
    private var menu03: SKLabelNode!
    private var menu04: SKLabelNode!
    private var menu05: SKLabelNode!
    private var menu06: SKLabelNode!
    private var menu07: SKLabelNode!
    private var menu08: SKLabelNode!
    private var menu09: SKLabelNode!
    
    let sand01 = SKShapeNode(circleOfRadius: 10)
    let sand02 = SKShapeNode(circleOfRadius: 10)
    
    override init(size: CGSize) {
        super.init(size: size)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        self.backgroundColor = UIColor(red: 0.15, green: 0.15, blue: 0.3, alpha: 1.0)
        
        menu01 = SKLabelNode(fontNamed: "Chalkduster")
        menu01.text = "Corpos Físicos - Volumes"
        menu01.fontSize = 40
        menu01.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.9)
        self.addChild(menu01)
        
        menu02 = SKLabelNode(fontNamed: "Chalkduster")
        menu02.text = "Corpos Físicos - Bordas"
        menu02.fontSize = 40
        menu02.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.8)
        self.addChild(menu02)
        
        menu03 = SKLabelNode(fontNamed: "Chalkduster")
        menu03.text = "Massa e Densidade"
        menu03.fontSize = 40
        menu03.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.7)
        self.addChild(menu03)
        
        menu04 = SKLabelNode(fontNamed: "Chalkduster")
        menu04.text = "Damping Linear e Angular"
        menu04.fontSize = 40
        menu04.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.6)
        self.addChild(menu04)
        
        menu05 = SKLabelNode(fontNamed: "Chalkduster")
        menu05.text = "Perda de Energia"
        menu05.fontSize = 40
        menu05.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.5)
        self.addChild(menu05)
        
        menu06 = SKLabelNode(fontNamed: "Chalkduster")
        menu06.text = "Rugosidade"
        menu06.fontSize = 40
        menu06.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.4)
        self.addChild(menu06)
        
        menu07 = SKLabelNode(fontNamed: "Chalkduster")
        menu07.text = "Impulso"
        menu07.fontSize = 40
        menu07.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.3)
        self.addChild(menu07)
        
        menu08 = SKLabelNode(fontNamed: "Chalkduster")
        menu08.text = "Força"
        menu08.fontSize = 40
        menu08.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.2)
        self.addChild(menu08)
        
        menu09 = SKLabelNode(fontNamed: "Chalkduster")
        menu09.text = "Colisão"
        menu09.fontSize = 40
        menu09.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height * 0.1)
        self.addChild(menu09)
        
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        sand01.physicsBody = SKPhysicsBody(circleOfRadius: sand01.frame.width/2)
        sand01.physicsBody!.restitution = 1.0
        sand01.position = CGPointMake(self.frame.width * 0.06, self.frame.height * 0.99)
        sand01.fillColor = UIColor.yellowColor()
        self.addChild(sand01)
        sand02.physicsBody = SKPhysicsBody(circleOfRadius: sand02.frame.width/2)
        sand02.physicsBody!.restitution = 1.0
        sand02.position = CGPointMake(self.frame.width * 0.94, self.frame.height * 0.99)
        sand02.fillColor = UIColor.cyanColor()
        self.addChild(sand02)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            var newScene: SKScene?
            if menu01.containsPoint(location) {
                newScene = Menu01(size: self.size)
            } else if menu02.containsPoint(location) {
                newScene = Menu02(size: self.size)
            } else if menu03.containsPoint(location) {
                newScene = Menu03(size: self.size)
            } else if menu04.containsPoint(location) {
                newScene = Menu04(size: self.size)
            } else if menu05.containsPoint(location) {
                newScene = Menu05(size: self.size)
            } else if menu06.containsPoint(location) {
                newScene = Menu06(size: self.size)
            } else if menu07.containsPoint(location) {
                newScene = Menu07(size: self.size)
            } else if menu08.containsPoint(location) {
                newScene = Menu08(size: self.size)
            } else if menu09.containsPoint(location) {
                newScene = Menu09(size: self.size)
            }
            if let scene = newScene {
                scene.scaleMode = .AspectFill
                self.view!.presentScene(scene)
            }
        }
    }
}
