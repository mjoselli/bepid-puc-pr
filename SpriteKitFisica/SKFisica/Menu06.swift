//
//  Menu05.swift
//  SKFisica
//
//  Created by Maicris Fernandes on 19/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import SpriteKit

class Menu06: SKScene {
    private let back = SKLabelNode(fontNamed: "Chalkduster")
    private let friction = SKLabelNode(fontNamed: "Chalkduster")
    private let incFriction = SKLabelNode(fontNamed: "Chalkduster")
    private let decFriction = SKLabelNode(fontNamed: "Chalkduster")
    private var key: SKSpriteNode!
    
    private var frictionValue: CGFloat = 0.0
    
    override init(size: CGSize) {
        super.init(size: size)
        createMenu()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createMenu() {
        back.text = "<<"
        back.fontSize = 60
        back.position = CGPointMake(self.size.width * 0.1, self.size.height * 0.9)
        self.addChild(back)
        
        incFriction.text = "<"
        incFriction.fontSize = 120
        incFriction.zRotation = CGFloat(-M_PI/2)
        incFriction.position = CGPointMake(self.size.width * 0.45, self.size.height * 0.95);
        self.addChild(incFriction)
        
        friction.text = "Rugosidade: ";
        friction.fontSize = 45;
        friction.position = CGPointMake(self.size.width * 0.5, self.size.height * 0.89);
        self.addChild(friction)
        
        decFriction.text = "<";
        decFriction.fontSize = 120;
        decFriction.zRotation = CGFloat(M_PI/2);
        decFriction.position = CGPointMake(self.size.width * 0.55, self.size.height * 0.85);
        self.addChild(decFriction)
        
        createEdge()
        createLine()
        createKey()
    }
    
    func createEdge() {
        let edge = SKShapeNode()
        let edgePath = CGPathCreateWithRect(self.frame, nil)
        edge.path = edgePath
        edge.strokeColor = UIColor.yellowColor()
        edge.lineWidth = 10.0
        edge.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        self.addChild(edge)
    }
    
    func createLine() {
        let line = SKShapeNode()
        let linePath = CGPathCreateMutable()
        CGPathMoveToPoint(linePath, nil, self.size.width, self.size.height * 0.7);
        CGPathAddLineToPoint(linePath, nil, self.size.width * 0.35, self.size.height * 0.5);
        line.path = linePath
        line.strokeColor = UIColor.redColor()
        line.lineWidth = 5.0
        line.physicsBody = SKPhysicsBody(edgeFromPoint: CGPointMake(self.size.width, self.size.height * 0.7),
            toPoint:CGPointMake(self.size.width * 0.35, self.size.height * 0.5))
        
        self.addChild(line)
    }
    
    func createKey() {
        key = SKSpriteNode(imageNamed: "Keys")
        key.physicsBody = SKPhysicsBody(rectangleOfSize: key.size)
        key.physicsBody!.dynamic = false
        key.name = "key"
        key.position = CGPointMake(self.frame.width * 0.9, self.frame.height * 0.75)
        key.physicsBody!.mass = 5
        self.addChild(key)
        frictionValue = key.physicsBody!.friction
        friction.text = String.localizedStringWithFormat("Friction: %.1f", frictionValue)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if back.containsPoint(location) {
                let scene = GameScene(size: self.size)
                self.view!.presentScene(scene)
            } else if key.containsPoint(location) {
                key.physicsBody!.dynamic = true
            } else if incFriction.containsPoint(location) {
                frictionValue += 0.1
                if frictionValue > 1.0 {
                    frictionValue = 1.0
                }
                friction.text = String.localizedStringWithFormat("Friction: %.1f", frictionValue)
                key.physicsBody!.friction = frictionValue
            } else if decFriction.containsPoint(location) {
                frictionValue -= 0.1
                if frictionValue < 0.0 {
                    frictionValue = 0.0
                }
                friction.text = String.localizedStringWithFormat("Friction: %.1f", frictionValue)
                key.physicsBody!.friction = frictionValue
            } else {
                self.reset()
            }
        }
    }
    
    func reset() {
        key.removeFromParent()
        self.createKey()
    }
    
    override func update(currentTime: NSTimeInterval) {
        if key.physicsBody!.resting {
            self.runAction(SKAction.waitForDuration(0.3), completion: { () -> Void in
                self.reset()
            })
        }
    }
}