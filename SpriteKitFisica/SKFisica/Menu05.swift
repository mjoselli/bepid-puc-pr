//
//  Menu05.swift
//  SKFisica
//
//  Created by Maicris Fernandes on 19/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import SpriteKit

class Menu05: SKScene {
    private let back = SKLabelNode(fontNamed: "Chalkduster")
    private let restitution = SKLabelNode(fontNamed: "Chalkduster")
    private let incRestitution = SKLabelNode(fontNamed: "Chalkduster")
    private let decRestitution = SKLabelNode(fontNamed: "Chalkduster")
    private var rock = SKSpriteNode(imageNamed: "Rocks")
    
    private var restitutionValue: CGFloat = 0.0
    
    override init(size: CGSize) {
        super.init(size: size)
        createMenu()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createMenu() {
        back.text = "<<"
        back.fontSize = 60
        back.position = CGPointMake(self.size.width * 0.1, self.size.height * 0.9)
        self.addChild(back)
        
        incRestitution.text = "<"
        incRestitution.fontSize = 120
        incRestitution.zRotation = CGFloat(-M_PI/2)
        incRestitution.position = CGPointMake(self.size.width * 0.45, self.size.height * 0.95);
        self.addChild(incRestitution)
        
        restitution.text = "Restitution: ";
        restitution.fontSize = 45;
        restitution.position = CGPointMake(self.size.width * 0.5, self.size.height * 0.89);
        self.addChild(restitution)
        
        decRestitution.text = "<";
        decRestitution.fontSize = 120;
        decRestitution.zRotation = CGFloat(M_PI/2);
        decRestitution.position = CGPointMake(self.size.width * 0.55, self.size.height * 0.85);
        self.addChild(decRestitution)
        
        createEdge()
        createRock()
    }
    
    func createEdge() {
        let edge = SKShapeNode()
        let edgePath = CGPathCreateWithRect(self.frame, nil)
        edge.path = edgePath
        edge.strokeColor = UIColor.yellowColor()
        edge.lineWidth = 10.0
        edge.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        self.addChild(edge)
    }
    
    func createRock() {
        rock.physicsBody = SKPhysicsBody(circleOfRadius: rock.frame.width/2)
        rock.physicsBody!.dynamic = false
        rock.name = "rock"
        rock.position = CGPointMake(self.frame.width * 0.5, self.frame.height * 0.75)
        self.addChild(rock)
        restitutionValue = rock.physicsBody!.restitution
        restitution.text = String.localizedStringWithFormat("Restitution: %.1f", restitutionValue)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if back.containsPoint(location) {
                let scene = GameScene(size: self.size)
                self.view!.presentScene(scene)
            } else if rock.containsPoint(location) {
                rock.physicsBody!.dynamic = true
            } else if incRestitution.containsPoint(location) {
                restitutionValue += 0.1
                if restitutionValue > 1.0 {
                    restitutionValue = 1.0
                }
                restitution.text = String.localizedStringWithFormat("Restitution: %.1f", restitutionValue)
                rock.physicsBody!.restitution = restitutionValue
            } else if decRestitution.containsPoint(location) {
                restitutionValue -= 0.1
                if restitutionValue < 0.0 {
                    restitutionValue = 0.0
                }
                restitution.text = String.localizedStringWithFormat("Restitution: %.1f", restitutionValue)
                rock.physicsBody!.restitution = restitutionValue
            } else {
                self.reset()
            }
        }
    }
    
    func reset() {
        rock.removeFromParent()
        self.createRock()
    }
    
    override func update(currentTime: NSTimeInterval) {
        if rock.physicsBody!.resting {
            self.runAction(SKAction.waitForDuration(0.3), completion: { () -> Void in
                self.reset()
            })
        }
    }
}