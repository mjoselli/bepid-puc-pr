//
//  Menu01.swift
//  SKFisica
//
//  Created by Maicris Fernandes on 18/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import SpriteKit

struct PhysicsCategory {
    static let  None: UInt32 = 0
    static let   Key: UInt32 = 0b1
    static let  Rock: UInt32 = 0b10
    static let Heart: UInt32 = 0b100
    static let  Edge: UInt32 = 0b1000
}

class Menu09: SKScene, SKPhysicsContactDelegate {
    private let back = SKLabelNode(fontNamed: "Chalkduster")
    private let desc01 = SKLabelNode(fontNamed: "Chalkduster")
    private let desc02 = SKLabelNode(fontNamed: "Chalkduster")
    private let key = SKSpriteNode(imageNamed: "Keys")
    private let rock = SKSpriteNode(imageNamed: "Rocks")
    private let heart = SKSpriteNode(imageNamed: "Hearts")
    
    private var objSelected = 0
    
    override init(size: CGSize) {
        super.init(size: size)
        createMenu()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createMenu() {
        back.text = "<<"
        back.fontSize = 60
        back.position = CGPointMake(self.size.width * 0.1, self.size.height * 0.9)
        self.addChild(back)
        
        desc01.text = "Chave colide com Rocha"
        desc01.fontSize = 25
        desc01.position = CGPointMake(self.size.width * 0.5, self.size.height * 0.7)
        self.addChild(desc01)
        
        desc02.text = "Rocha notifica colisão com Coração"
        desc02.fontSize = 25
        desc02.position = CGPointMake(self.size.width * 0.5, self.size.height * 0.62)
        self.addChild(desc02)
        
        key.position = CGPointMake(self.size.width * 0.3, self.size.height * 0.9)
        self.addChild(key)
        
        rock.position = CGPointMake(self.size.width * 0.55, self.size.height * 0.9)
        self.addChild(rock)
        
        heart.position = CGPointMake(self.size.width * 0.85, self.size.height * 0.9)
        self.addChild(heart)
        
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        self.physicsBody!.categoryBitMask = PhysicsCategory.Edge
        
        self.physicsWorld.contactDelegate = self
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if back.containsPoint(location) {
                let scene = GameScene(size: self.size)
                self.view!.presentScene(scene)
            } else if key.containsPoint(location) {
                objSelected = 0
            } else if rock.containsPoint(location) {
                objSelected = 1
            } else if heart.containsPoint(location) {
                objSelected = 2
            } else {
                switch (objSelected) {
                case 0:
                    createKey(location)
                case 1:
                    createRock(location)
                case 2:
                    createHeart(location)
                default:
                    createKey(location)
                }
            }
        }
    }
    
    func createKey(location: CGPoint) {
        let newKey = SKSpriteNode(imageNamed: "Keys")
        newKey.physicsBody = SKPhysicsBody(rectangleOfSize: newKey.size)
        newKey.physicsBody!.categoryBitMask = PhysicsCategory.Key
        newKey.physicsBody!.collisionBitMask = PhysicsCategory.Edge | PhysicsCategory.Rock
        newKey.name = "key"
        newKey.position = location
        
        self.addChild(newKey)
    }
    
    func createRock(location: CGPoint) {
        let newRock = SKSpriteNode(imageNamed: "Rocks")
        newRock.physicsBody = SKPhysicsBody(circleOfRadius: newRock.size.width/2)
        newRock.physicsBody!.categoryBitMask = PhysicsCategory.Rock
        newRock.physicsBody!.collisionBitMask = PhysicsCategory.Key | PhysicsCategory.Edge | PhysicsCategory.Heart
        newRock.physicsBody!.contactTestBitMask = PhysicsCategory.Heart
        newRock.name = "rock"
        newRock.position = location
        
        self.addChild(newRock)
    }
    
    func createHeart(location: CGPoint) {
        let newHeart = SKSpriteNode(imageNamed: "Hearts")
        let heartPath = CGPathCreateMutable()
        CGPathMoveToPoint(heartPath, nil, 0, -newHeart.size.height/2);
        CGPathAddLineToPoint(heartPath, nil, newHeart.size.width/2, 0);
        CGPathAddLineToPoint(heartPath, nil, newHeart.size.width/2, newHeart.size.height/2);
        CGPathAddLineToPoint(heartPath, nil, -newHeart.size.width/2, newHeart.size.height/2);
        CGPathAddLineToPoint(heartPath, nil, -newHeart.size.width/2, 0);
        CGPathAddLineToPoint(heartPath, nil, 0, -newHeart.size.height/2);
        newHeart.physicsBody = SKPhysicsBody(polygonFromPath: heartPath)
        newHeart.physicsBody!.categoryBitMask = PhysicsCategory.Heart
        newHeart.physicsBody!.collisionBitMask = PhysicsCategory.Edge
        newHeart.name = "heart"
        newHeart.position = location
        
        self.addChild(newHeart)
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        let collision = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        if collision == PhysicsCategory.Rock | PhysicsCategory.Heart {
            contact.bodyA.node!.removeFromParent()
            contact.bodyB.node!.removeFromParent()
        }
    }
    
    override func update(currentTime: NSTimeInterval) {
        let obj = [key, rock, heart]
        for var i = 0; i < 3; i++ {
            if objSelected == i {
                obj[i].alpha = 1.0
            } else {
                obj[i].alpha = 0.5
            }
        }
    }
}
