//
//  Menu07.swift
//  SKFisica
//
//  Created by Maicris Fernandes on 19/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import SpriteKit

class Menu07 : SKScene {
    private let back = SKLabelNode(fontNamed: "Chalkduster")
    private let x = SKLabelNode(fontNamed: "Chalkduster")
    private let incX = SKLabelNode(fontNamed: "Chalkduster")
    private let decX = SKLabelNode(fontNamed: "Chalkduster")
    private let y = SKLabelNode(fontNamed: "Chalkduster")
    private let incY = SKLabelNode(fontNamed: "Chalkduster")
    private let decY = SKLabelNode(fontNamed: "Chalkduster")
    private let key = SKSpriteNode(imageNamed: "Keys")
    private let point = SKSpriteNode(imageNamed: "sand")
    
    private var xValue: CGFloat = 0.0
    private var yValue: CGFloat = 0.0
    private var posImpulse: CGPoint!
    private var selPos = 4
    private var pos = [SKSpriteNode]()
    
    override init(size: CGSize) {
        super.init(size: size)
        createMenu()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createMenu() {
        back.text = "<<"
        back.fontSize = 60
        back.position = CGPointMake(self.size.width * 0.1, self.size.height * 0.9)
        self.addChild(back)
        
        incX.text = "<"
        incX.fontSize = 120
        incX.zRotation = CGFloat(-M_PI/2)
        incX.position = CGPointMake(self.size.width * 0.31, self.size.height * 0.95);
        self.addChild(incX)
        
        x.text = "x: 0.0";
        x.fontSize = 45;
        x.position = CGPointMake(self.size.width * 0.35, self.size.height * 0.89);
        self.addChild(x)
        
        decX.text = "<";
        decX.fontSize = 120;
        decX.zRotation = CGFloat(M_PI/2);
        decX.position = CGPointMake(self.size.width * 0.4, self.size.height * 0.85);
        self.addChild(decX)
        
        incY.text = "<";
        incY.fontSize = 120;
        incY.zRotation = CGFloat(-M_PI/2);
        incY.position = CGPointMake(self.size.width * 0.76, self.size.height * 0.95);
        self.addChild(incY)
        
        y.text = "y: 0.0";
        y.fontSize = 45;
        y.position = CGPointMake(self.size.width * 0.8, self.size.height * 0.89);
        self.addChild(y)
        
        decY.text = "<";
        decY.fontSize = 120;
        decY.zRotation = CGFloat(M_PI/2);
        decY.position = CGPointMake(self.size.width * 0.85, self.size.height * 0.85);
        self.addChild(decY)
        
        createEdge()
        createPosRect()
        createKey()
        
        self.addChild(point)
    }
    
    func createEdge() {
        let edge = SKShapeNode()
        let edgePath = CGPathCreateWithRect(self.frame, nil)
        edge.path = edgePath
        edge.strokeColor = UIColor.yellowColor()
        edge.lineWidth = 10.0
        edge.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        self.addChild(edge)
    }
    
    func createPosRect() {
        let posRectPath = CGPathCreateWithRect(CGRectMake(0, 0, 80, 80), nil)
        let posRect = SKShapeNode(path: posRectPath)
        posRect.strokeColor = UIColor.lightGrayColor()
        posRect.lineWidth = 2.0
        posRect.position = CGPointMake(self.size.width * 0.52, self.size.height * 0.87)
        self.addChild(posRect)
        for i in 0..<9 {
            pos += [SKSpriteNode(imageNamed: "sand")]
        }
        for i in 0..<3 {
            for j in 0..<3 {
                pos[i + j * 3].position = CGPointMake(
                    self.size.width * 0.52 + CGFloat(i * 40),
                    self.size.height * 0.87 + CGFloat(j * 40))
                pos[i + j * 3].setScale(3)
                self.addChild(pos[i + j * 3])
            }
            
        }
    }
    
    func createKey() {
        key.physicsBody = SKPhysicsBody(rectangleOfSize: key.size)
        key.name = "key"
        key.position = CGPointMake(self.frame.width * 0.9, self.frame.height * 0.7)
        self.addChild(key)
        posImpulse = CGPointMake(key.position.x, key.size.height/2)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if back.containsPoint(location) {
                let scene = GameScene(size: self.size)
                self.view!.presentScene(scene)
            } else if incX.containsPoint(location) {
                xValue += 5.0
                x.text = String.localizedStringWithFormat("x: %.1f", xValue)
            } else if decX.containsPoint(location) {
                xValue -= 5.0
                x.text = String.localizedStringWithFormat("x: %.1f", xValue)
            } else if incY.containsPoint(location) {
                yValue += 5.0
                y.text = String.localizedStringWithFormat("y: %.1f", yValue)
            } else if decY.containsPoint(location) {
                yValue -= 5.0
                y.text = String.localizedStringWithFormat("y: %.1f", yValue)
            } else if pos[0].containsPoint(location) {
                selPos = 0
            } else if pos[1].containsPoint(location) {
                selPos = 1
            } else if pos[2].containsPoint(location) {
                selPos = 2
            } else if pos[3].containsPoint(location) {
                selPos = 3
            } else if pos[4].containsPoint(location) {
                selPos = 4
            } else if pos[5].containsPoint(location) {
                selPos = 5
            } else if pos[6].containsPoint(location) {
                selPos = 6
            } else if pos[7].containsPoint(location) {
                selPos = 7
            } else if pos[8].containsPoint(location) {
                selPos = 8
            } else {
                key.physicsBody?.applyImpulse(CGVectorMake(xValue, yValue), atPoint: posImpulse)
            }
        }
    }
    
    override func update(currentTime: NSTimeInterval) {
        for i in 0..<3 {
            for j in 0..<3 {
                if i + j * 3 == selPos {
                    pos[i + j * 3].alpha = 1.0
                } else {
                    pos[i + j * 3].alpha = 0.3
                }
            }
        }
        switch (selPos) {
        case 0:
            posImpulse = CGPointMake(key.position.x - key.size.width / 2, key.position.y - key.size.height / 2)
        case 1:
            posImpulse = CGPointMake(key.position.x, key.position.y - key.size.height / 2)
        case 2:
            posImpulse = CGPointMake(key.position.x + key.size.width / 2, key.position.y - key.size.height / 2)
        case 3:
            posImpulse = CGPointMake(key.position.x - key.size.width / 2, key.position.y)
        case 4:
            posImpulse = CGPointMake(key.position.x, key.position.y)
        case 5:
            posImpulse = CGPointMake(key.position.x + key.size.width / 2, key.position.y)
        case 6:
            posImpulse = CGPointMake(key.position.x - key.size.width / 2, key.position.y + key.size.height / 2)
        case 7:
            posImpulse = CGPointMake(key.position.x, key.position.y + key.size.height / 2)
        case 8:
            posImpulse = CGPointMake(key.position.x + key.size.width / 2, key.position.y + key.size.height / 2)
        default:
            posImpulse = CGPointZero
        }
        point.position = posImpulse
    }
}