//
//  Menu02.swift
//  SKFisica
//
//  Created by Maicris Fernandes on 18/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import SpriteKit

class Menu02: SKScene {
    private let back = SKLabelNode(fontNamed: "Chalkduster")
    
    override init(size: CGSize) {
        super.init(size: size)
        createMenu()
        createEdge()
        createTrap()
        createLine()
        createArc()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createMenu() {
        back.text = "<<"
        back.fontSize = 60
        back.position = CGPointMake(self.size.width * 0.1, self.size.height * 0.9)
        self.addChild(back)
    }
    
    func createEdge() {
        let edge = SKShapeNode()
        let edgePath = CGPathCreateWithRect(self.frame, nil)
        edge.path = edgePath
        edge.strokeColor = UIColor.yellowColor()
        edge.lineWidth = 10.0
        edge.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        self.addChild(edge)
    }
    
    func createTrap() {
        let trap = SKShapeNode()
        let trapPath = CGPathCreateMutable()
        CGPathMoveToPoint(trapPath, nil, self.size.width * 0.7, 0);
        CGPathAddLineToPoint(trapPath, nil, self.size.width, 0);
        CGPathAddLineToPoint(trapPath, nil, self.size.width, self.size.height * 0.1);
        CGPathAddLineToPoint(trapPath, nil, self.size.width * 0.7, self.size.height * 0.05);
        CGPathCloseSubpath(trapPath);
        trap.path = trapPath
        trap.strokeColor = UIColor.blueColor()
        trap.lineWidth = 5.0
        trap.physicsBody = SKPhysicsBody(edgeLoopFromPath: trapPath)
        
        self.addChild(trap)
    }
    
    func createLine() {
        let line = SKShapeNode()
        let linePath = CGPathCreateMutable()
        CGPathMoveToPoint(linePath, nil, self.size.width, self.size.height * 0.7);
        CGPathAddLineToPoint(linePath, nil, self.size.width * 0.35, self.size.height * 0.5);
        line.path = linePath
        line.strokeColor = UIColor.redColor()
        line.lineWidth = 5.0
        line.physicsBody = SKPhysicsBody(edgeFromPoint: CGPointMake(self.size.width, self.size.height * 0.7),
            toPoint:CGPointMake(self.size.width * 0.35, self.size.height * 0.5))
        
        self.addChild(line)
    }
    
    func createArc() {
        let arc = SKShapeNode()
        let arcPath = CGPathCreateMutable()
        CGPathMoveToPoint(arcPath, nil, 0, self.size.height * 0.5);
        CGPathAddArc(arcPath, nil, self.size.width * 0.5, self.size.height * 0.5, self.size.width/2,
            CGFloat(M_PI), CGFloat(3*M_PI/2), false);
        arc.physicsBody = SKPhysicsBody(edgeChainFromPath: arcPath)
        arc.path = arcPath
        arc.strokeColor = UIColor.greenColor()
        arc.lineWidth = 5.0
        
        self.addChild(arc)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if back.containsPoint(location) {
                let scene = GameScene(size: self.size)
                self.view!.presentScene(scene)
            } else {
                createRock(location)
            }
        }
    }
    
    func createRock(location: CGPoint) {
        let newRock = SKSpriteNode(imageNamed: "Rocks")
        newRock.physicsBody = SKPhysicsBody(circleOfRadius: newRock.size.width/2)
        newRock.physicsBody!.linearDamping = 0.0
        newRock.name = "rock"
        newRock.position = location
        
        self.addChild(newRock)
    }
}