//
//  Menu04.swift
//  SKFisica
//
//  Created by Maicris Fernandes on 19/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import SpriteKit

class Menu04: SKScene {
    private let back = SKLabelNode(fontNamed: "Chalkduster")
    private let angular = SKLabelNode(fontNamed: "Chalkduster")
    private let incAngular = SKLabelNode(fontNamed: "Chalkduster")
    private let decAngular = SKLabelNode(fontNamed: "Chalkduster")
    private let linear = SKLabelNode(fontNamed: "Chalkduster")
    private let incLinear = SKLabelNode(fontNamed: "Chalkduster")
    private let decLinear = SKLabelNode(fontNamed: "Chalkduster")
    private var rock = SKSpriteNode(imageNamed: "Rocks")
    
    private var linearValue: CGFloat = 1.0
    private var angularValue: CGFloat = 1.0
    
    override init(size: CGSize) {
        super.init(size: size)
        createMenu()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createMenu() {
        back.text = "<<"
        back.fontSize = 60
        back.position = CGPointMake(self.size.width * 0.1, self.size.height * 0.9)
        self.addChild(back)
        
        incLinear.text = "<"
        incLinear.fontSize = 120
        incLinear.zRotation = CGFloat(-M_PI/2)
        incLinear.position = CGPointMake(self.size.width * 0.31, self.size.height * 0.95);
        self.addChild(incLinear)
        
        linear.text = "Mass: ";
        linear.fontSize = 45;
        linear.position = CGPointMake(self.size.width * 0.35, self.size.height * 0.89);
        self.addChild(linear)
        
        decLinear.text = "<";
        decLinear.fontSize = 120;
        decLinear.zRotation = CGFloat(M_PI/2);
        decLinear.position = CGPointMake(self.size.width * 0.4, self.size.height * 0.85);
        self.addChild(decLinear)
        
        incAngular.text = "<";
        incAngular.fontSize = 120;
        incAngular.zRotation = CGFloat(-M_PI/2);
        incAngular.position = CGPointMake(self.size.width * 0.71, self.size.height * 0.95);
        self.addChild(incAngular)
        
        angular.text = "Density: ";
        angular.fontSize = 45;
        angular.position = CGPointMake(self.size.width * 0.75, self.size.height * 0.89);
        self.addChild(angular)
        
        decAngular.text = "<";
        decAngular.fontSize = 120;
        decAngular.zRotation = CGFloat(M_PI/2);
        decAngular.position = CGPointMake(self.size.width * 0.8, self.size.height * 0.85);
        self.addChild(decAngular)
        
        createEdge()
        createTrap()
        createLine()
        createArc()
        createRock()
    }
    
    func createEdge() {
        let edge = SKShapeNode()
        let edgePath = CGPathCreateWithRect(self.frame, nil)
        edge.path = edgePath
        edge.strokeColor = UIColor.yellowColor()
        edge.lineWidth = 10.0
        edge.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        self.addChild(edge)
    }
    
    func createTrap() {
        let trap = SKShapeNode()
        let trapPath = CGPathCreateMutable()
        CGPathMoveToPoint(trapPath, nil, self.size.width * 0.7, 0);
        CGPathAddLineToPoint(trapPath, nil, self.size.width, 0);
        CGPathAddLineToPoint(trapPath, nil, self.size.width, self.size.height * 0.1);
        CGPathAddLineToPoint(trapPath, nil, self.size.width * 0.7, self.size.height * 0.05);
        CGPathCloseSubpath(trapPath);
        trap.path = trapPath
        trap.strokeColor = UIColor.blueColor()
        trap.lineWidth = 5.0
        trap.physicsBody = SKPhysicsBody(edgeLoopFromPath: trapPath)
        
        self.addChild(trap)
    }
    
    func createLine() {
        let line = SKShapeNode()
        let linePath = CGPathCreateMutable()
        CGPathMoveToPoint(linePath, nil, self.size.width, self.size.height * 0.7);
        CGPathAddLineToPoint(linePath, nil, self.size.width * 0.35, self.size.height * 0.5);
        line.path = linePath
        line.strokeColor = UIColor.redColor()
        line.lineWidth = 5.0
        line.physicsBody = SKPhysicsBody(edgeFromPoint: CGPointMake(self.size.width, self.size.height * 0.7),
            toPoint:CGPointMake(self.size.width * 0.35, self.size.height * 0.5))
        
        self.addChild(line)
    }
    
    func createArc() {
        let arc = SKShapeNode()
        let arcPath = CGPathCreateMutable()
        CGPathMoveToPoint(arcPath, nil, 0, self.size.height * 0.5);
        CGPathAddArc(arcPath, nil, self.size.width * 0.5, self.size.height * 0.5, self.size.width/2,
            CGFloat(M_PI), CGFloat(3*M_PI/2), false);
        arc.physicsBody = SKPhysicsBody(edgeChainFromPath: arcPath)
        arc.path = arcPath
        arc.strokeColor = UIColor.greenColor()
        arc.lineWidth = 5.0
        
        self.addChild(arc)
    }
    
    func createRock() {
        rock.physicsBody = SKPhysicsBody(circleOfRadius: rock.frame.width/2)
        rock.physicsBody!.dynamic = false
        rock.name = "rock"
        rock.position = CGPointMake(self.frame.width * 0.8, self.frame.height * 0.75)
        self.addChild(rock)
        linearValue = rock.physicsBody!.linearDamping
        linear.text = String.localizedStringWithFormat("Linear: %.1f", linearValue)
        angularValue = rock.physicsBody!.angularDamping
        angular.text = String.localizedStringWithFormat("Angular: %.1f", angularValue)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if back.containsPoint(location) {
                let scene = GameScene(size: self.size)
                self.view!.presentScene(scene)
            } else if rock.containsPoint(location) {
                rock.physicsBody!.dynamic = true
            } else if incLinear.containsPoint(location) {
                linearValue += 0.1
                if linearValue > 1.0 {
                    linearValue = 1.0
                }
                linear.text = String.localizedStringWithFormat("Linear: %.1f", linearValue)
                rock.physicsBody!.linearDamping = linearValue
            } else if decLinear.containsPoint(location) {
                linearValue -= 0.1
                if linearValue < 0.0 {
                    linearValue = 0.0
                }
                linear.text = String.localizedStringWithFormat("Linear: %.1f", linearValue)
                rock.physicsBody!.linearDamping = linearValue
            } else if incAngular.containsPoint(location) {
                angularValue += 0.1
                if angularValue > 1.0 {
                    angularValue = 1.0
                }
                angular.text = String.localizedStringWithFormat("Angular: %.1f", angularValue)
                rock.physicsBody!.angularDamping = angularValue
            } else if decAngular.containsPoint(location) {
                angularValue -= 0.1
                if angularValue < 0.0 {
                    angularValue = 0.0
                }
                angular.text = String.localizedStringWithFormat("Angular: %.1f", angularValue)
                rock.physicsBody!.angularDamping = angularValue
            }
        }
    }
    
    override func update(currentTime: NSTimeInterval) {
        if rock.physicsBody!.resting {
            self.runAction(SKAction.waitForDuration(0.3), completion: { () -> Void in
                self.rock.removeFromParent()
                self.createRock()
            })
        }
    }
}