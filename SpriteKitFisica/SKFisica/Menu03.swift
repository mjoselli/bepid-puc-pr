//
//  Menu03.swift
//  SKFisica
//
//  Created by Maicris Fernandes on 18/05/15.
//  Copyright (c) 2015 Melhor Idéia. All rights reserved.
//

import SpriteKit

class Menu03: SKScene {
    private let back = SKLabelNode(fontNamed: "Chalkduster")
    private let density = SKLabelNode(fontNamed: "Chalkduster")
    private let incDensity = SKLabelNode(fontNamed: "Chalkduster")
    private let decDensity = SKLabelNode(fontNamed: "Chalkduster")
    private let mass = SKLabelNode(fontNamed: "Chalkduster")
    private let incMass = SKLabelNode(fontNamed: "Chalkduster")
    private let decMass = SKLabelNode(fontNamed: "Chalkduster")
    private let sandTex = SKTexture(imageNamed: "sand")
    private var rock = SKSpriteNode(imageNamed: "Rocks")
    
    private var massValue: CGFloat = 1.0
    private var densityValue: CGFloat = 1.0
    
    override init(size: CGSize) {
        super.init(size: size)
        createMenu()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createMenu() {
        back.text = "<<"
        back.fontSize = 60
        back.position = CGPointMake(self.size.width * 0.1, self.size.height * 0.9)
        self.addChild(back)
        
        incMass.text = "<"
        incMass.fontSize = 120
        incMass.zRotation = CGFloat(-M_PI/2)
        incMass.position = CGPointMake(self.size.width * 0.31, self.size.height * 0.95);
        self.addChild(incMass)
        
        mass.text = "Mass: ";
        mass.fontSize = 45;
        mass.position = CGPointMake(self.size.width * 0.35, self.size.height * 0.89);
        self.addChild(mass)
        
        decMass.text = "<";
        decMass.fontSize = 120;
        decMass.zRotation = CGFloat(M_PI/2);
        decMass.position = CGPointMake(self.size.width * 0.4, self.size.height * 0.85);
        self.addChild(decMass)
        
        incDensity.text = "<";
        incDensity.fontSize = 120;
        incDensity.zRotation = CGFloat(-M_PI/2);
        incDensity.position = CGPointMake(self.size.width * 0.71, self.size.height * 0.95);
        self.addChild(incDensity)
        
        density.text = "Density: ";
        density.fontSize = 45;
        density.position = CGPointMake(self.size.width * 0.75, self.size.height * 0.89);
        self.addChild(density)
        
        decDensity.text = "<";
        decDensity.fontSize = 120;
        decDensity.zRotation = CGFloat(M_PI/2);
        decDensity.position = CGPointMake(self.size.width * 0.8, self.size.height * 0.85);
        self.addChild(decDensity)
        
        createEdge()
        createSands()
        createRock()
    }
    
    func createEdge() {
        let edge = SKShapeNode()
        let edgePath = CGPathCreateWithRect(self.frame, nil)
        edge.path = edgePath
        edge.strokeColor = UIColor.yellowColor()
        edge.lineWidth = 10.0
        edge.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        
        self.addChild(edge)
    }

    func createSands() {
        for i in 1...200 {
            let sand = SKSpriteNode(texture: sandTex)
            sand.position = CGPointMake(CGFloat(arc4random()) % self.size.width , self.size.height * 0.6 + CGFloat(arc4random()) % 200)
            sand.physicsBody = SKPhysicsBody(circleOfRadius: sand.frame.width/2)
            sand.physicsBody!.mass = 0.3;
            sand.setScale(2)
            sand.name = "sand";
            
            self.addChild(sand)
        }
    }
    
    func createRock() {
        rock.physicsBody = SKPhysicsBody(circleOfRadius: rock.frame.width/2)
        rock.physicsBody!.linearDamping = 0.0;
        rock.name = "rock";
        rock.position = CGPointMake(self.frame.width/2, self.frame.height * 0.75)
        rock.physicsBody!.dynamic = false;
        self.addChild(rock)
        rock.physicsBody!.mass = massValue;
        mass.text = String.localizedStringWithFormat("Mass: %.1f", massValue)
        densityValue = rock.physicsBody!.density;
        density.text = String.localizedStringWithFormat("Density: %.1f", densityValue)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if back.containsPoint(location) {
                let scene = GameScene(size: self.size)
                self.view!.presentScene(scene)
            } else if rock.containsPoint(location) {
                rock.physicsBody!.dynamic = true
            } else if incMass.containsPoint(location) {
                massValue += 1.0
                mass.text = String.localizedStringWithFormat("Mass: %.1f", massValue)
                rock.physicsBody!.mass = massValue
                densityValue = rock.physicsBody!.density
                density.text = String.localizedStringWithFormat("Density: %.1f", densityValue)
            } else if decMass.containsPoint(location) {
                massValue -= 1.0
                if massValue < 1.0 {
                    massValue = 1.0
                }
                mass.text = String.localizedStringWithFormat("Mass: %.1f", massValue)
                rock.physicsBody!.mass = massValue
                densityValue = rock.physicsBody!.density
                density.text = String.localizedStringWithFormat("Density: %.1f", densityValue)
            } else if incDensity.containsPoint(location) {
                densityValue += 1.0
                density.text = String.localizedStringWithFormat("Density: %.1f", densityValue)
                rock.physicsBody!.density = densityValue
                massValue = rock.physicsBody!.mass
                mass.text = String.localizedStringWithFormat("Mass: %.1f", massValue)
            } else if decDensity.containsPoint(location) {
                densityValue -= 1.0
                if densityValue < 1.0 {
                    densityValue = 1.0
                }
                density.text = String.localizedStringWithFormat("Density: %.1f", densityValue)
                rock.physicsBody!.density = densityValue
                massValue = rock.physicsBody!.mass
                mass.text = String.localizedStringWithFormat("Mass: %.1f", massValue)
            }
        }
    }
    
    override func update(currentTime: NSTimeInterval) {
        if rock.physicsBody!.resting {
            self.runAction(SKAction.waitForDuration(0.3), completion: { () -> Void in
                self.removeAllChildren()
                self.createMenu()
            })
        }
    }
}