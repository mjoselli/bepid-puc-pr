//
//  Repository.swift
//  CoreDataSample
//
//  Created by Vinícius Godoy on 17/06/15.
//  Copyright (c) 2015 BEPiD. All rights reserved.
//

import UIKit
import CoreData

class Repository: NSObject {
    static let shared = Repository()
    
    private (set) var contacts = [Contact]()
    
    override init() {
        super.init()
        loadData()
    }
    
    private func loadData() -> Bool{
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!

        let fetchRequest = NSFetchRequest(entityName:"Contact")
        var error: NSError?
        
        let fetchedResults = managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            contacts.removeAll(keepCapacity: false)
            for result in results {
                let contact = Contact(
                    name: result.valueForKey("name") as! String!,
                    City: result.valueForKey("city") as! String!
                )
                contact.id = result.objectID
                contacts.append(contact)
            }
            return true;
        }
        
        println("Could not fetch \(error), \(error!.userInfo)")
        return false
    }
    
    func saveData(contact : Contact) -> Bool {
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        let entity = NSEntityDescription.entityForName("Contact",
            inManagedObjectContext:
            managedContext)
        let person = NSManagedObject(entity: entity!, insertIntoManagedObjectContext:managedContext);

        
        person.setValue(contact.name, forKey: "name")
        person.setValue(contact.city, forKey: "city")
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
            return false
        }
        
        contact.id = person.objectID;
        contacts.append(contact)
        return true
    }
    
    func removeData(index : Int) -> Bool {
        let contact = contacts[index];
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        var error : NSError?
        let managedContact : NSManagedObject? = managedContext.existingObjectWithID(contact.id!, error: &error)
        if managedContact != nil {
            managedContext.deleteObject(managedContact!);
            if managedContext.save(&error) {
                contacts.removeAtIndex(index);
                return true;
            }
        }
        
        return false;
    }
}
