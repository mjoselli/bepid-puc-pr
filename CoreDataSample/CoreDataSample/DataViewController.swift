//
//  ContactsViewController.swift
//  CoreDataSample
//
//  Created by Vinícius Godoy on 17/06/15.
//  Copyright (c) 2015 BEPiD. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var city: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAddContact(sender: UIBarButtonItem) {
        navigationController?.popToRootViewControllerAnimated(true)
    }

    @IBAction func addContact(sender: AnyObject) {
        if (count(name!.text) == 0) {
            let alert = UIAlertView(title: "Campo obrigatório",
                message: "Nome", delegate: nil,
                cancelButtonTitle: "Ok")
            alert.show()
            name.becomeFirstResponder()
        } else if (count(city!.text) == 0) {
            let alert = UIAlertView(title: "Campo obrigatório",
                message: "Cidade", delegate: nil,
                cancelButtonTitle: "Ok")
            alert.show()
            city.becomeFirstResponder()
        } else {
            let contact = Contact(name: name!.text, City: city!.text);
            if (Repository.shared.saveData(contact)) {
                name!.text = "";
                city!.text = "";
                name.becomeFirstResponder();
            }
            navigationController?.popToRootViewControllerAnimated(true);
        }
    }
}
