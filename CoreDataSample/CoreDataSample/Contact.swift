//
//  Contact.swift
//  CoreDataSample
//
//  Created by Vinícius Godoy on 17/06/15.
//  Copyright (c) 2015 BEPiD. All rights reserved.
//

import UIKit
import CoreData

class Contact {
    var id : NSManagedObjectID?
    var name : String
    var city : String
    
    init(name : String, City city: String) {
        self.name = name
        self.city = city
    }
}
