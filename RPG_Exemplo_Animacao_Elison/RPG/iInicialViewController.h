//
//  ERViewController.h
//  RPGTeste
//
//  Created by Elison Rissatto on 28/10/13.
//  Copyright (c) 2013 Elison Rissatto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iInicialViewController : UIViewController

@property UIWindow *window;

@property CGPoint currentPoint;

@property (weak, nonatomic) IBOutlet UIImageView *imageCima;
@property (weak, nonatomic) IBOutlet UIImageView *imageBaixo;

+(iInicialViewController *)getInicialViewController;

@end
