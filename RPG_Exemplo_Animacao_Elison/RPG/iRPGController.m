//
//  iRPGController.m
//  RPG
//
//  Created by Elison Rissatto on 15/10/13.
//  Copyright (c) 2013 Elison Rissatto. All rights reserved.
//

#import "iRPGController.h"

@interface iRPGController()

- (id)init;

@end

@implementation iRPGController

static iRPGController *rpgController; // Estático da classe ....

- (id)init
{
    return [super init];
}

+(iRPGController *)getRPGController // Singleton
{
    static iRPGController *rpgController  = nil; // ?
    if (!rpgController) {
        rpgController = [[super allocWithZone:nil]init];
    }
    return rpgController;
}

@end
