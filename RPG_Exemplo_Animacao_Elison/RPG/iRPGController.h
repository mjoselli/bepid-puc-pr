//
//  iRPGController.h
//  RPG
//
//  Created by Elison Rissatto on 15/10/13.
//  Copyright (c) 2013 Elison Rissatto. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MAX_CRITICAL 2

@interface iRPGController : NSObject

+ (iRPGController *)getRPGController;

@end
