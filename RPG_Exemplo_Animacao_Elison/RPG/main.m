//
//  main.m
//  RPG
//
//  Created by Elison Rissatto on 15/10/13.
//  Copyright (c) 2013 Elison Rissatto. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([iAppDelegate class]));
    }
}
