//
//  iAppDelegate.m
//  RPG
//
//  Created by Elison Rissatto on 15/10/13.
//  Copyright (c) 2013 Elison Rissatto. All rights reserved.
//

#import "iAppDelegate.h"

#import "iInicialViewController.h"
#import "iRPGController.h"

@implementation iAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    iInicialViewController *ivc = [iInicialViewController getInicialViewController];
    
    ivc.window = self.window;
    
    [self.window setRootViewController:ivc];

    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
