//
//  iAppDelegate.h
//  RPG
//
//  Created by Elison Rissatto on 15/10/13.
//  Copyright (c) 2013 Elison Rissatto. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>

@interface iAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
