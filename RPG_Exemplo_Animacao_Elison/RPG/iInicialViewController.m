//
//  ERViewController.m
//  RPGTeste
//
//  Created by Elison Rissatto on 28/10/13.
//  Copyright (c) 2013 Elison Rissatto. All rights reserved.
//

#import "iInicialViewController.h"

@interface iInicialViewController ()

@property CGRect frameOriginal;
@property BOOL lockView;

@end

@implementation iInicialViewController

static iInicialViewController *singleton; // Estático da classe ....

+(iInicialViewController *)getInicialViewController // Singleton
{
    static iInicialViewController *singleton  = nil; // ?
    if (!singleton) {
        singleton = [[super allocWithZone:nil]init];
    }
    return singleton;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.lockView = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.frameOriginal = self.imageCima.frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    self.currentPoint = [[touches anyObject] locationInView:self.view];
}

- (void) touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    if (!self.lockView) {
        CGPoint activePoint = [[touches anyObject] locationInView:self.view];
        
        CGPoint newPoint = CGPointMake(self.imageCima.center.x + (activePoint.x - self.currentPoint.x),
                                       self.imageCima.center.y + (activePoint.y - self.currentPoint.y));
        
        // If too far right...
        if (newPoint.x > (self.view.bounds.size.width))
            newPoint.x = (self.view.bounds.size.width);
        else if (newPoint.x < 0)  // If too far left...
            newPoint.x = 0;
        
        // If too far down...
        if (newPoint.y > (self.view.bounds.size.height))
            newPoint.y = (self.view.bounds.size.height);
        else if (newPoint.y < 0)  // If too far up...
            newPoint.y = 0;
        
        self.imageCima.center = newPoint;
        
        self.currentPoint = activePoint;
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (!self.lockView) {
        CGPoint center = self.imageBaixo.center;
        
        CGRect middle = CGRectMake(center.x-10,center.y-10,20,20);
        
        if(CGRectContainsPoint(middle, self.imageCima.center)) {
            self.lockView = YES;
            [UIView animateWithDuration:1.0
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 self.imageCima.frame = self.imageBaixo.frame;
                             }
                             completion:^(BOOL finished){
                                 [UIView animateWithDuration:1.0
                                                  animations:^{
                                                      [self.view setBackgroundColor:[UIColor whiteColor]];
                                                      [self.view setBackgroundColor:[UIColor blackColor]];
                                                  }
                                                  completion:^(BOOL finished) {
                                                      [self vaiParaSalaUm];
                                                  }];}];
        }
    }
}

- (void)vaiParaSalaUm {
    iInicialViewController *inicial = [iInicialViewController getInicialViewController];
    
    inicial.window = self.window;
    
    [UIView animateWithDuration:2.0
                     animations:^{
                         [self.imageBaixo setAlpha:0.0];
                         [self.imageCima setAlpha:0.0];
                     }
                     completion:^(BOOL finished) {
                         [self.window setRootViewController:inicial];
                         self.imageCima.frame = self.frameOriginal;
                         [self.imageBaixo setAlpha:1.0];
                         [self.imageCima setAlpha:1.0];
                         self.lockView = NO;
                     }];
}

@end
