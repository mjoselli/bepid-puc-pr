//
//  ArmasTableViewController.m
//  RPG
//
//  Created by Mark Joselli on 3/18/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "ArmasTableViewController.h"
#import "MyTableViewCell.h"

@interface ArmasTableViewController (){
    NSMutableDictionary *tipos;
    NSArray *tiposKeys;
}

@end

@implementation ArmasTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    tipos = [[NSMutableDictionary alloc]init];
    [tipos setObject:@[@"Espada",@"Arco"] forKey:@"Ataque"];
    [tipos setObject:@[@"Escudo"] forKey:@"Defesa"];
    tiposKeys = [tipos allKeys];
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"RPGBG.jpg"]];
    [tempImageView setFrame:self.tableView.frame];
    
    self.tableView.backgroundView = tempImageView;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return tiposKeys[section];
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.contentView.backgroundColor = [UIColor colorWithRed:0.6f green:0.4f blue:0.2f alpha:1.f];
        //view.backgroundColor = [UIColor colorWithRed:0.6f green:0.4f blue:0.2f alpha:1.f];
        tableViewHeaderFooterView.textLabel.textColor = [UIColor whiteColor];
        tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:@"Papyrus" size:17.0f];
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return tiposKeys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSArray *armas =[tipos objectForKey:tiposKeys[section]];
    return armas.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    NSString *tipo = tiposKeys[indexPath.section];
    NSArray *armas = [tipos objectForKey:tipo];
    // Configure the cell...
   // cell.textLabel.text = armas[indexPath.row];
    
    
    
    cell.labelArma.text = armas[indexPath.row];
    cell.imageArma.image = [UIImage imageNamed:@"breno.png"];
    cell.textViewArma.text = @"Olá Mundo!";
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *tipo = tiposKeys[indexPath.section];
    NSArray *armas = [tipos objectForKey:tipo];

    NSLog(@"Selected:%@",armas[indexPath.row]);
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
