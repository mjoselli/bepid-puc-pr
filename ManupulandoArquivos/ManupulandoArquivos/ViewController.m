//
//  ViewController.m
//  ManupulandoArquivos
//
//  Created by Mark Joselli on 3/13/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "ViewController.h"
#import "FileManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSString *bundleContent = [FileManager LoadStringFromFile:@"texto.txt" useBundle:YES];
    NSLog(@"Lendo do bundle: %@",bundleContent);
    
    BOOL sucesso = [FileManager SaveString:@"escrevendo no arquivo" inFile:@"arquivo.txt"];
    
    NSLog(@"Arquivo Salvo:%@",sucesso ? @"Sim" : @"Não");
    
    NSString *pastaContent = [FileManager LoadStringFromFile:@"arquivo.txt" useBundle:NO];
    NSLog(@"Lendo da pasta:%@",pastaContent);
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
