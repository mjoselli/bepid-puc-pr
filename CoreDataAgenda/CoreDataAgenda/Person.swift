//
//  Person.swift
//  
//
//  Created by Maicris Fernandes on 27/06/15.
//
//

import Foundation
import CoreData

class Person: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var phones: NSSet

}
