//
//  ViewController.swift
//  CoreDataAgenda
//
//  Created by Maicris Fernandes on 27/06/15.
//  Copyright (c) 2015 BEPiD. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var btnChange: UIBarButtonItem!
    
    var op = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        tableview.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if op == 0 {
            if let list = DataStore.sharedInstance.getPersonList() {
                return list.count
            }
        } else {
            if let list = DataStore.sharedInstance.getPhoneList() {
                return list.count
            }
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        }
        
        if op == 0 {
            if let list = DataStore.sharedInstance.getPersonList() {
                let person = list[indexPath.row] as Person
                cell!.textLabel!.text = person.name
                cell!.detailTextLabel?.text = "Telefones cadastrados: \(person.phones.count)"
            }
        } else {
            if let list = DataStore.sharedInstance.getPhoneList() {
                let phone = list[indexPath.row] as Phone
                cell!.textLabel!.text = phone.number
                cell!.detailTextLabel?.text = "Contato Relacionado: \(phone.person.name)"
            }
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            if op == 0 {
                if DataStore.sharedInstance.removePersoAtRow(indexPath.row) {
                    tableview.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
                }
            } else {
                
            }
        }
    }
    
    @IBAction func change(sender: AnyObject) {
        if let t = btnChange.title {
            if t == "Telefones" {
                btnChange.title = "Contatos"
                op = 1
            } else {
                btnChange.title = "Telefones"
                op = 0
            }
            tableview.reloadData()
        }
    }
}

