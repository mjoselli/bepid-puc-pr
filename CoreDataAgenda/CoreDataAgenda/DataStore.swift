//
//  DataStore.swift
//  CoreDataAgenda
//
//  Created by Maicris Fernandes on 27/06/15.
//  Copyright (c) 2015 BEPiD. All rights reserved.
//

import UIKit
import CoreData

class DataStore {
    
    let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext!
    
    class var sharedInstance: DataStore {
        struct Static {
            static let instance: DataStore = DataStore()
        }
        return Static.instance
    }
    
    func getPersonList() -> [Person]? {
        
        let request = NSFetchRequest(entityName: "Person")
        var error: NSError?
        let personList = managedContext.executeFetchRequest(request, error: &error) as! [Person]?
        
        return personList
    }
    
    func getPhoneList() -> [Phone]? {
        
        let request = NSFetchRequest(entityName: "Phone")
        var error: NSError?
        let phoneList = managedContext.executeFetchRequest(request, error: &error) as! [Phone]?
        
        return phoneList
    }
    
    func savePerso(Name name: String, Phones phones: [String]) -> Bool {
        
        let personEntity = NSEntityDescription.entityForName("Person", inManagedObjectContext: managedContext)
        let person = Person(entity: personEntity!, insertIntoManagedObjectContext: managedContext)
        person.name = name
        
        for phoneNumber: String in phones{
            let phoneEntity = NSEntityDescription.entityForName("Phone", inManagedObjectContext: managedContext)
            let phone = Phone(entity: phoneEntity!, insertIntoManagedObjectContext: managedContext)
            phone.number = phoneNumber
            var personPhones = person.phones.mutableCopy() as! NSMutableSet
            personPhones.addObject(phone)
            person.phones = personPhones.copy() as! NSSet
        }
        var error: NSError?
        if !managedContext.save(&error) {
            return false
        } else {
            return true
        }
    }
    
    func removePersoAtRow(row: Int) -> Bool {
        
        let personList = self.getPersonList()
        if let list = personList {
            let person = list[row]
            managedContext.deleteObject(person)
            var error: NSError?
            if !managedContext.save(&error) {
                return false
            } else {
                return true
            }
        }
        
        return false
    }
    
    func removePhoneAtRow(row: Int) {
        
    }
}
