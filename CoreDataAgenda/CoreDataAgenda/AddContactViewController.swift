//
//  AddContactViewController.swift
//  CoreDataAgenda
//
//  Created by Maicris Fernandes on 27/06/15.
//  Copyright (c) 2015 BEPiD. All rights reserved.
//

import UIKit

class AddContactViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var phones = [String]()

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var pickPhone: UIPickerView!
    @IBOutlet weak var btnAddPhone: UIButton!
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return phones.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return phones[row]
    }
    
    @IBAction func AddPhone(sender: AnyObject) {
        phones.append(txtPhone.text)
        txtPhone.text = ""
        txtPhone.resignFirstResponder()
        pickPhone.reloadAllComponents()
    }
    
    @IBAction func back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func save(sender: AnyObject) {
        if !DataStore.sharedInstance.savePerso(Name: txtName.text, Phones: phones) {
            println("Falha ao tentar salvar este contato")
        } else {
            println("Contato salvo com sucesso")
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
