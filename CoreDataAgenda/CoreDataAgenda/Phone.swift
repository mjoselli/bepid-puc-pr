//
//  Phone.swift
//  
//
//  Created by Maicris Fernandes on 27/06/15.
//
//

import Foundation
import CoreData

class Phone: NSManagedObject {

    @NSManaged var number: String
    @NSManaged var person: Person

}
