//
//  ExemploMoveEase.swift
//  ExemploActions
//

import SpriteKit

class ExemploMoveEase: SKScene {
    
    let btnVoltar : SKSpriteNode
    let bolaVermelha : Elemento
    let bolaAzul : Elemento
    let bolaCinza : Elemento
    let bolaRosa : Elemento
    var esquerda : Bool = false
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        bolaVermelha = Elemento(tipo: "bola", cor: SKColor .redColor(), texto: "EaseIn")
        bolaAzul = Elemento(tipo: "bola", cor: SKColor .blueColor(), texto: "EaseInEaseOut")
        bolaCinza = Elemento(tipo: "bola", cor: SKColor .grayColor(), texto: "EaseOut")
        bolaRosa = Elemento(tipo: "bola", cor: SKColor .purpleColor(), texto: "Normal")
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = SKColor .blackColor()
        
        let texto : SKLabelNode
        texto = SKLabelNode(fontNamed:"Chalkduster")
        texto.text = "Toque na tela para mover as bolas"
        texto.fontColor = SKColor .whiteColor()
        texto.fontSize = 50
        texto.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.95)
        self.addChild(texto)
        
        bolaVermelha.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.8)
        self.addChild(bolaVermelha)
        
        bolaAzul.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.6)
        self.addChild(bolaAzul)
        
        bolaCinza.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.4)
        self.addChild(bolaCinza)
        
        bolaRosa.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.2)
        self.addChild(bolaRosa)
        
        btnVoltar.position = CGPoint(x: 100, y: 1950)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = Menu(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }

            
            bolaVermelha.removeAllActions()
            bolaAzul.removeAllActions()
            bolaCinza.removeAllActions()
            bolaRosa.removeAllActions()
            
            if(esquerda) {
                
                let actionVermelhoMoveTo = SKAction.moveToX(self.size.width * 0.9, duration: 1)
                actionVermelhoMoveTo.timingMode = SKActionTimingMode.EaseIn
                bolaVermelha.runAction(actionVermelhoMoveTo)
                
                let actionAzulMoveTo = SKAction.moveToX(self.size.width * 0.9, duration: 1)
                actionAzulMoveTo.timingMode = SKActionTimingMode.EaseInEaseOut
                bolaAzul.runAction(actionAzulMoveTo)
                
                let actionCinzaMoveTo = SKAction.moveToX(self.size.width * 0.9, duration: 1)
                actionCinzaMoveTo.timingMode = SKActionTimingMode.EaseOut
                bolaCinza.runAction(actionCinzaMoveTo)
                
                let actionRosaMoveTo = SKAction.moveToX(self.size.width * 0.9, duration: 1)
                bolaRosa.runAction(actionRosaMoveTo)
                
                esquerda = false
            }
            else {
                esquerda = true
                
                let actionVermelhoMoveTo = SKAction.moveToX(self.size.width * 0.1, duration: 1)
                actionVermelhoMoveTo.timingMode = SKActionTimingMode.EaseIn
                bolaVermelha.runAction(actionVermelhoMoveTo)
                
                let actionAzulMoveTo = SKAction.moveToX(self.size.width * 0.1, duration: 1)
                actionAzulMoveTo.timingMode = SKActionTimingMode.EaseInEaseOut
                bolaAzul.runAction(actionAzulMoveTo)
                
                let actionCinzaMoveTo = SKAction.moveToX(self.size.width * 0.1, duration: 1)
                actionCinzaMoveTo.timingMode = SKActionTimingMode.EaseOut
                bolaCinza.runAction(actionCinzaMoveTo)
                
                let actionRosaMoveTo = SKAction.moveToX(self.size.width * 0.1, duration: 1)
                bolaRosa.runAction(actionRosaMoveTo)
            }
            
        }
    }
}