//
//  ExemploFollowPath.swift
//  ExemploActions
//

import SpriteKit

class ExemploFollowPath: SKScene {
    
    let btnVoltar : SKSpriteNode
    let bola : Elemento
    let quadrado : UIBezierPath
    var movendo : Bool = false
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        bola = Elemento(tipo: "bola")
        quadrado = UIBezierPath(rect: CGRectMake(0, 0, 500, 500))
        
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = SKColor .blackColor()
        
        bola.position = CGPoint(x: CGRectGetMidX(self.frame) - quadrado.bounds.height/2, y:CGRectGetMidY(self.frame) - quadrado.bounds.width/2)
        self.addChild(bola)
        
        let actionFollowPath = SKAction.followPath(quadrado.CGPath, asOffset: true, orientToPath: false, duration: 3)
        bola.runAction(SKAction .repeatActionForever(actionFollowPath))
        
        btnVoltar.position = CGPoint(x: 100, y: 1950)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = Menu(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
        }
    }
}
