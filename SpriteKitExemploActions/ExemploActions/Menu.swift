//
//  Menu.swift
//  ExemploActions
//

import SpriteKit

class Menu : SKScene {
    
    let menu01 : SKLabelNode
    let menu02 : SKLabelNode
    let menu03 : SKLabelNode
    let menu04 : SKLabelNode
    let menu05 : SKLabelNode
    let menu06 : SKLabelNode
    let menu07 : SKLabelNode
    let menu08 : SKLabelNode
    
    override init(size: CGSize) {
        
        menu01 = SKLabelNode(fontNamed:"Chalkduster")
        menu02 = SKLabelNode(fontNamed:"Chalkduster")
        menu03 = SKLabelNode(fontNamed:"Chalkduster")
        menu04 = SKLabelNode(fontNamed:"Chalkduster")
        menu05 = SKLabelNode(fontNamed:"Chalkduster")
        menu06 = SKLabelNode(fontNamed:"Chalkduster")
        menu07 = SKLabelNode(fontNamed:"Chalkduster")
        menu08 = SKLabelNode(fontNamed:"Chalkduster")
        
        super.init(size: size)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        view.backgroundColor = UIColor .blackColor()
        
        menu01.text = "Move"
        menu01.fontColor = SKColor .whiteColor()
        menu01.fontSize  = 70
        menu01.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.9)
        self.addChild(menu01)
        
        menu02.text = "Move com Ease"
        menu02.fontColor = SKColor .whiteColor()
        menu02.fontSize  = 70
        menu02.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.8)
        self.addChild(menu02)
        
        menu03.text = "Follow Path"
        menu03.fontColor = SKColor .whiteColor()
        menu03.fontSize  = 70
        menu03.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.7)
        self.addChild(menu03)
        
        menu04.text = "Rotate"
        menu04.fontColor = SKColor .whiteColor()
        menu04.fontSize  = 70
        menu04.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.6)
        self.addChild(menu04)
        
        menu05.text = "Speed"
        menu05.fontColor = SKColor .whiteColor()
        menu05.fontSize  = 70
        menu05.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.5)
        self.addChild(menu05)
        
        menu06.text = "Scale"
        menu06.fontColor = SKColor .whiteColor()
        menu06.fontSize  = 70
        menu06.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.4)
        self.addChild(menu06)
        
        menu07.text = "Fade"
        menu07.fontColor = SKColor .whiteColor()
        menu07.fontSize  = 70
        menu07.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.3)
        self.addChild(menu07)
        
        menu08.text = "Sequence"
        menu08.fontColor = SKColor .whiteColor()
        menu08.fontSize  = 70
        menu08.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.2)
        self.addChild(menu08)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            if(menu01 .containsPoint(location)) {
                let novaCena = ExemploMoveTo(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu02 .containsPoint(location)) {
                let novaCena = ExemploMoveEase(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu03 .containsPoint(location)) {
                let novaCena = ExemploFollowPath(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu04 .containsPoint(location)) {
                let novaCena = ExemploRotate(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu05 .containsPoint(location)) {
                let novaCena = ExemploSpeed(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu06 .containsPoint(location)) {
                let novaCena = ExemploScale(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu07 .containsPoint(location)) {
                let novaCena = ExemploFade(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu08 .containsPoint(location)) {
                let novaCena = ExemploSequence(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
        }
    }
}