//
//  Elemento.swift
//  ExemploActions
//

import SpriteKit

class Elemento : SKNode {
    
    let tipo : String
    let cor : SKColor
    let texto : String
    var elemento : SKShapeNode!
    var legenda : SKLabelNode!
    
    init(tipo: String) {
        self.tipo = tipo
        self.cor = SKColor .redColor()
        self.texto = ""
        super.init()
        criaElemento()
    }
    
    init(tipo: String, cor: SKColor) {
        self.tipo = tipo
        self.cor = cor
        self.texto = ""
        super.init()
        criaElemento()
    }
    
    init(tipo: String, cor: SKColor, texto: String) {
        self.tipo = tipo
        self.cor = cor
        self.texto = texto
        super.init()
        criaElemento()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func criaElemento() {
        if(tipo == "bola") {
            elemento = SKShapeNode(circleOfRadius: 100.0)
        }
        else if(tipo == "quadrado") {
            elemento = SKShapeNode(path: (UIBezierPath(rect: CGRectMake(0, 0, 100, 100))).CGPath, centered: true)
        }
        
        elemento.fillColor = cor
        elemento.strokeColor = SKColor .whiteColor()
        elemento.glowWidth = 0.5
        self.addChild(elemento)
        
        if(texto != "") {
            legenda = SKLabelNode(fontNamed:"Chalkduster")
            legenda.text = texto
            legenda.fontColor = SKColor .whiteColor()
            legenda.fontSize = 30
            legenda.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
            legenda.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
            self.addChild(legenda)
        }
    }
}