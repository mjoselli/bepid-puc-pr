//
//  ExemploRotate.swift
//  ExemploActions
//

import SpriteKit

class ExemploRotate: SKScene {
    
    let btnVoltar : SKSpriteNode
    let quadrado : Elemento
    var movendo : Bool = false
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        quadrado = Elemento(tipo: "quadrado")
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = SKColor .blackColor()
        
        let texto : SKLabelNode
        texto = SKLabelNode(fontNamed:"Chalkduster")
        texto.text = "Toque na tela para mover o quadrado"
        texto.fontColor = SKColor .whiteColor()
        texto.fontSize = 50
        texto.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.95)
        self.addChild(texto)
        
        quadrado.position = CGPoint(x: CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        
        self.addChild(quadrado)
        
        let actionRotacao = SKAction.rotateByAngle(2, duration: 0.5)
        quadrado.runAction(SKAction .repeatActionForever(actionRotacao))
        
        btnVoltar.position = CGPoint(x: 100, y: 1950)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            
            let location = touch.locationInNode(self)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = Menu(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
            
            if(movendo) {
                quadrado.removeActionForKey("movimento")
                movendo = false
            }
            
            movendo = true
            let actionMoveTo = SKAction.moveTo(location, duration: 1)
            quadrado.runAction(actionMoveTo, withKey:"movimento")
        }
    }
}
