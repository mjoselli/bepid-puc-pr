//
//  ExemploSpeed.swift
//  ExemploActions
//

import SpriteKit

class ExemploSpeed: SKScene {
    
    let btnVoltar : SKSpriteNode
    let bola : Elemento
    let quadrado : UIBezierPath
    var ativo : Bool = false
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        bola = Elemento(tipo: "bola")
        quadrado = UIBezierPath(rect: CGRectMake(0, 0, 500, 500))
        
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = SKColor .blackColor()
        
        let texto : SKLabelNode
        texto = SKLabelNode(fontNamed:"Chalkduster")
        texto.text = "Toque na tela para alterar a velocidade"
        texto.fontColor = SKColor .whiteColor()
        texto.fontSize = 50
        texto.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.95)
        self.addChild(texto)
        
        bola.position = CGPoint(x: CGRectGetMidX(self.frame) - quadrado.bounds.height/2, y:CGRectGetMidY(self.frame) - quadrado.bounds.width/2)
        self.addChild(bola)
        
        let actionFollowPath = SKAction.followPath(quadrado.CGPath, asOffset: true, orientToPath: false, duration: 3)
        bola.runAction(SKAction .repeatActionForever(actionFollowPath))
        
        btnVoltar.position = CGPoint(x: 100, y: 1950)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = Menu(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
            
            if(ativo) {
                ativo = false
                bola.removeActionForKey("speed")
                let actionSpeedRapido = SKAction.speedTo(1.5, duration: 0.5)
                bola.runAction(actionSpeedRapido, withKey:"speed")
            }
            else {
                ativo = true
                let actionSpeedDevagar = SKAction.speedTo(0.5, duration: 0.5)
                bola.runAction(actionSpeedDevagar, withKey:"speed")
            }
        }
    }
}
