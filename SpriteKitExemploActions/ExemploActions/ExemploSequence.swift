//
//  ExemploSequence.swift
//  ExemploActions
//

import SpriteKit

class ExemploSequence: SKScene {
    
    let btnVoltar : SKSpriteNode
    let quadrado : Elemento
    var emAcao : Bool = false
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        quadrado = Elemento(tipo: "quadrado")
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = SKColor .blackColor()
        
        let texto : SKLabelNode
        texto = SKLabelNode(fontNamed:"Chalkduster")
        texto.text = "Toque na tela para..."
        texto.fontColor = SKColor .whiteColor()
        texto.fontSize = 50
        texto.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.95)
        self.addChild(texto)
        
        quadrado.position = CGPoint(x: CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        self.addChild(quadrado)
        
        btnVoltar.position = CGPoint(x: 100, y: 1950)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = Menu(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
            
            if(!emAcao) {
                emAcao = true
                let actionMoveTo = SKAction.moveTo(location, duration: 1)
                actionMoveTo.timingMode = SKActionTimingMode.EaseInEaseOut
                
                let actionScaleDiminui = SKAction.scaleTo(0.5, duration: 0.5)
                let actionRotaciona = SKAction.rotateByAngle(2.0, duration: 0.5)
                let actionSequenciaUnica = SKAction.sequence([actionScaleDiminui, actionRotaciona])
                
                let actionFadeEsconde = SKAction.fadeAlphaTo(0.5, duration: 0.6)
                let delay = SKAction.waitForDuration(0.2)
                let actionFadeMostra = SKAction.fadeAlphaTo(1.0, duration: 0.6)
                let actionSequenciaInfinita = SKAction.sequence([actionFadeEsconde, delay, actionFadeMostra, delay])
                
                quadrado.runAction(actionMoveTo, completion: {
                    
                    let actionScaleAumenta = SKAction.scaleTo(1.0, duration: 0.5)
                    self.quadrado.runAction(actionScaleAumenta)
                    self.emAcao = false
                })
                
                quadrado.runAction(actionSequenciaUnica)
                quadrado.runAction(SKAction .repeatActionForever(actionSequenciaInfinita))
            }
        }
    }
}