//
//  ExemploCropNode.swift
//  AulaSpriteKit
//

import SpriteKit

class ExemploCropNode : SKScene {
    
    let btnVoltar : SKSpriteNode
    let mascara : SKSpriteNode
    var mascaraFull = false
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        mascara = SKSpriteNode(color: UIColor .blackColor(), size: CGSizeMake(100, 100))
        super.init(size: size)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "introBackground")
        
        var crop = SKCropNode()
        crop.addChild(background)
        crop.maskNode = mascara
        crop.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        self.addChild(crop)
        
        btnVoltar.position = CGPoint(x: 120, y: 1420)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            var touchNode = nodeAtPoint(location)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = NavegacaoCenas(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
            else {
                if(mascaraFull) {
                    mascaraFull = false
                    var actionMascara = SKAction .scaleTo(1.0, duration: 0.7)
                    mascara .runAction(actionMascara)
                }
                else {
                    mascaraFull = true
                    var actionMascara = SKAction .scaleTo(25.0, duration: 0.7)
                    mascara .runAction(actionMascara)
                }
            }
        }
    }
}