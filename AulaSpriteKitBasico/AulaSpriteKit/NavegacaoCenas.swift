//
//  NavegacaoCenas.swift
//  AulaSpriteKit
//

import SpriteKit

class NavegacaoCenas : SKScene {
    
    let menu01 : SKLabelNode
    let menu02 : SKLabelNode
    let menu03 : SKLabelNode
    let menu04 : SKLabelNode
    let menu05 : SKLabelNode
    let menu06 : SKLabelNode
    
    override init(size: CGSize) {
        
        menu01 = SKLabelNode(fontNamed:"Chalkduster")
        menu02 = SKLabelNode(fontNamed:"Chalkduster")
        menu03 = SKLabelNode(fontNamed:"Chalkduster")
        menu04 = SKLabelNode(fontNamed:"Chalkduster")
        menu05 = SKLabelNode(fontNamed:"Chalkduster")
        menu06 = SKLabelNode(fontNamed:"Chalkduster")
        
        super.init(size: size)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        view.backgroundColor = UIColor .blackColor()
        
        let background = SKSpriteNode(imageNamed: "introBackground")
        background.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        self.addChild(background)
        background.alpha = 0.3
        
        menu01.text = "Texto e Imagem"
        menu01.fontColor = SKColor .whiteColor()
        menu01.fontSize  = 70
        menu01.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.9)
        self.addChild(menu01)
        
        menu02.text = "Animação de Atlas"
        menu02.fontColor = SKColor .whiteColor()
        menu02.fontSize  = 70
        menu02.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.75)
        self.addChild(menu02)
        
        menu03.text = "Animação de Atlas e do Fundo"
        menu03.fontColor = SKColor .whiteColor()
        menu03.fontSize  = 70
        menu03.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.60)
        self.addChild(menu03)
        
        menu04.text = "Eventos de Toque"
        menu04.fontColor = SKColor .whiteColor()
        menu04.fontSize  = 70
        menu04.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.45)
        self.addChild(menu04)
        
        menu05.text = "Partículas"
        menu05.fontColor = SKColor .whiteColor()
        menu05.fontSize  = 70
        menu05.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.30)
        self.addChild(menu05)
        
        menu06.text = "Crop Node"
        menu06.fontColor = SKColor .whiteColor()
        menu06.fontSize  = 70
        menu06.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.15)
        self.addChild(menu06)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            if(menu01 .containsPoint(location)) {
                let novaCena = TextoEImagem(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.crossFadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu02 .containsPoint(location)) {
                let novaCena = AnimaAtlas(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.fadeWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu03 .containsPoint(location)) {
                let novaCena = AnimaAtlasEFundo(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.fadeWithColor(UIColor .redColor(), duration: 1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu04 .containsPoint(location)) {
                let novaCena = EventosDeToque(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.flipHorizontalWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu05 .containsPoint(location)) {
                let novaCena = Particulas(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.flipVerticalWithDuration(1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
            else if(menu06 .containsPoint(location)) {
                let novaCena = ExemploCropNode(size:self.size)
                novaCena.scaleMode = scaleMode
                let reveal = SKTransition.revealWithDirection(SKTransitionDirection.Down, duration: 1.5)
                self.view?.presentScene(novaCena, transition: reveal)
            }
        }
    }
}