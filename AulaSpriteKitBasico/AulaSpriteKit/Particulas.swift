//
//  Particulas.swift
//  AulaSpriteKit
//

import SpriteKit

class Particulas : SKScene {
    
    let velocidadeFundo = 90.0
    let velocidadeChao = 30.0
    let velocidadeFrames = 0.08
    
    let btnVoltar : SKSpriteNode
    
    override init(size: CGSize) {
        
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        super.init(size: size)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        //Background
        let background = SKNode()
        
        let background01 = SKSpriteNode(imageNamed: "introBackground")
        background01.position = CGPoint(x:0, y:0)
        background.addChild(background01)
        
        let background02 = SKSpriteNode(imageNamed: "introBackground")
        background02.position = CGPoint(x: 2048, y: 0)
        background02.xScale = -1
        background.addChild(background02)
        
        let background03 = SKSpriteNode(imageNamed: "introBackground")
        background03.position = CGPoint(x:4096, y:0)
        background.addChild(background03)
        
        background.position = CGPoint(x: 1024, y:CGRectGetMidY(self.frame))
        self.addChild(background)
        
        let moveBackground = SKAction.moveToX(-3072, duration: velocidadeFundo)
        let reiniciaBackground = SKAction.moveToX(1024, duration: 0)
        background.runAction(SKAction.repeatActionForever(
            SKAction.sequence([moveBackground, reiniciaBackground]))
        )
        
        
        //Chao
        let chao = SKNode()
        
        let chao01 = SKSpriteNode(imageNamed: "chao")
        chao01.position = CGPoint(x:0, y:0)
        chao.addChild(chao01)
        
        let chao02 = SKSpriteNode(imageNamed: "chao")
        chao02.position = CGPoint(x: 2048, y: 0)
        chao02.xScale = -1
        chao.addChild(chao02)
        
        let chao03 = SKSpriteNode(imageNamed: "chao")
        chao03.position = CGPoint(x:4096, y:0)
        chao.addChild(chao03)
        
        chao.position = CGPoint(x: 1024, y:chao01.size.height/2)
        self.addChild(chao)
        
        let moveChao = SKAction.moveToX(-3072, duration: velocidadeChao)
        let reiniciaChao = SKAction.moveToX(1024, duration: 0)
        chao.runAction(SKAction.repeatActionForever(
            SKAction.sequence([moveChao, reiniciaChao])))
        
        
        //Personagem
        let personagem = Personagem()
        personagem.position = CGPoint(x:CGRectGetMidX(self.frame), y:600)
        addChild(personagem)
        personagem.alteraEstadoPersonagem("andando")
        
        
        //Particula
        let emitterNode = SKEmitterNode(fileNamed: "Fumaca.sks")
        emitterNode.position = CGPointMake(CGRectGetMidX(self.frame) + 15, 465)
        self.addChild(emitterNode)
        
        btnVoltar.position = CGPoint(x: 120, y: 1420)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            var touchNode = nodeAtPoint(location)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = NavegacaoCenas(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorsCloseHorizontalWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
        }
    }
}