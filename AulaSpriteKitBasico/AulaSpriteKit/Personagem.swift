//
//  Personagem.swift
//  AulaSpriteKit
//

import SpriteKit

class Personagem : SKNode {
    
    let velocidadeFrames = 0.08
    let velocidade : CGFloat = 230.0
    
    var direcao : CGFloat = 1.0
    
    var size = CGSize()
    
    var personagem : SKSpriteNode!
    var esperandoFrames = [SKTexture]()
    var andandoFrames = [SKTexture]()
    var esperandoAction: SKAction!
    var andandoAction: SKAction!
    
    override init() {
        super.init()
        criaPersonagem()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Criacao do personagem
    private func criaPersonagem() {
        personagem = SKSpriteNode(imageNamed: "mark-sprite-Idle0")
        personagem.position = CGPoint(x:0 , y:0)
        self.addChild(personagem)
        size = personagem.size
        esperandoAction = criaPersonagemEsperando()
        andandoAction = criaPersonagemAndando()
        personagemEspera()
    }
    
    private func criaPersonagemEsperando() -> SKAction {
        let personagemAnimacoesAtlas = SKTextureAtlas(named: "idle")
        let totalImagens = personagemAnimacoesAtlas.textureNames.count
        for var i = 0; i < totalImagens; i++ {
            let personagemTextureName = "mark-sprite-Idle\(i)"
            esperandoFrames.append(personagemAnimacoesAtlas.textureNamed(personagemTextureName))
        }
        return SKAction.repeatActionForever(
            SKAction.animateWithTextures(esperandoFrames,timePerFrame: velocidadeFrames))
    }
    
    private func criaPersonagemAndando() -> SKAction {
        let personagemAnimacoesAtlas = SKTextureAtlas(named: "walk")
        
        let numImages = personagemAnimacoesAtlas.textureNames.count
        for var i = 0; i < numImages; i++ {
            let personagemTextureName = "mark-sprite-Walk\(i)"
            andandoFrames.append(personagemAnimacoesAtlas.textureNamed(personagemTextureName))
        }
        
        return SKAction.repeatActionForever(
            SKAction.animateWithTextures(andandoFrames,timePerFrame: velocidadeFrames))
    }
    
    //Acoes do personagem
    private func personagemCaminha() {
        personagem.removeAllActions()
        personagem.runAction(andandoAction)
    }
    
    private func personagemEspera() {
        personagem.removeAllActions()
        personagem.runAction(esperandoAction)
    }
    
    func alteraEstadoPersonagem(estado:String) {
        
        self.xScale = direcao
        
        switch(estado) {
            case "esperando":
                personagemEspera()
            case "andando":
                personagemCaminha()
            default:
                personagemEspera()
        }
    }
}
