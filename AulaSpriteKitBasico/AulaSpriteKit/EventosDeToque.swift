//
//  GameScene.swift
//  AulaSpriteKit
//

import SpriteKit

class EventosDeToque: SKScene {
    
    var personagem = Personagem()
    
    let btnVoltar : SKSpriteNode
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        super.init(size: size)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "introBackground")
        background.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        self.addChild(background)
        
        let chao = SKSpriteNode(imageNamed: "chao")
        chao.position = CGPoint(x: CGRectGetMidX(self.frame), y:chao.size.height/2)
        self.addChild(chao)
        
        let texto = SKLabelNode(fontNamed:"Chalkduster")
        texto.text = "Toque para mover o personagem"
        texto.fontColor = SKColor .whiteColor()
        texto.fontSize  = 50
        texto.position = CGPoint(x: CGRectGetMidX(self.frame), y: self.size.height * 0.93)
        self.addChild(texto)
        
        personagem.position = CGPoint(x: personagem.size.width/2, y: 610)
        self.addChild(personagem)
        
        btnVoltar.position = CGPoint(x: 120, y: 1420)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
    }
    
    //Evento de touch
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            var touchNode = nodeAtPoint(location)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = NavegacaoCenas(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
            else {
                personagem.removeAllActions()
                if(location.x < personagem.position.x) {
                    personagem.direcao = -1 //Esquerda
                }
                else {
                    personagem.direcao = 1 //Direita
                }
                
                var dist : CGFloat = sqrt(pow(personagem.position.x - location.x, 2.0))
                var time : CGFloat = dist / personagem.velocidade;
                
                let action = SKAction.moveToX(location.x, duration: Double(time) )
                personagem.runAction(action, completion: {
                    self.personagem.alteraEstadoPersonagem("esperando")
                })
                personagem.alteraEstadoPersonagem("andando")
            }
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
