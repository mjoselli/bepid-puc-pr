//
//  TextoEImagem.swift
//  AulaSpriteKit
//

import SpriteKit

class TextoEImagem: SKScene {
    
    let btnVoltar : SKSpriteNode
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        view.backgroundColor = UIColor .blackColor()
        
        let background = SKSpriteNode(imageNamed: "introBackground")
        background.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        self.addChild(background)
        background.alpha = 0.3
        
        let titleLabel = SKLabelNode(fontNamed:"Chalkduster")
        titleLabel.text = "O Cavaleiro Andarilho"
        titleLabel.fontColor = SKColor .whiteColor()
        titleLabel.fontSize = 100
        titleLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        
        self.addChild(titleLabel)
        
        btnVoltar.position = CGPoint(x: 120, y: 1420)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
        
        let myPath = CGPathCreateMutable();
        CGPathAddArc(myPath, nil, 0, 0, 150, 0, CGFloat(M_PI * 2), true)
        let ball = SKShapeNode(path: myPath)
        ball.lineWidth = 1.0;
        ball.fillColor = UIColor.blueColor();
        ball.strokeColor = UIColor.whiteColor()
        ball.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        
        self.addChild(ball)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            var touchNode = nodeAtPoint(location)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = NavegacaoCenas(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
        }
    }
}
