//
//  AnimaAtlas.swift
//  AulaSpriteKit
//

import SpriteKit

class AnimaAtlas: SKScene {
    
    let velocidadeFrames = 0.08
    
    let btnVoltar : SKSpriteNode
    
    override init(size: CGSize) {
        btnVoltar = SKSpriteNode(imageNamed: "btnVoltar")
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "introBackground")
        background.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        self.addChild(background)
        
        let chao = SKSpriteNode(imageNamed: "chaoInicio")
        chao.position = CGPoint(x:self.size.width - chao.size.width/2, y:chao.size.height/2)
        self.addChild(chao)
        
        btnVoltar.position = CGPoint(x: 120, y: 1420)
        btnVoltar.name = "btnVoltar"
        self.addChild(btnVoltar)
        
        //Recuperando atlas
        let personagemAnimacoesAtlas = SKTextureAtlas(named: "idle")
        
        //Array para armazenar todas as imagens do atlas
        var esperandoFrames = [SKTexture]()
        
        //Atlas é um array e podemos pegar seu tamanho
        let totalImagens = personagemAnimacoesAtlas.textureNames.count
        
        //Recuperando texturas que estão no atlas
        for var i = 0; i < totalImagens; i++ {
            let personagemTextureName = "mark-sprite-Idle\(i)"
            
            //Para animar, precisamos recuperar todas as imagens. Então vamos colocá-las em um array
            esperandoFrames.append(personagemAnimacoesAtlas.textureNamed(personagemTextureName))
        }
        
        let primeiroFrame = esperandoFrames[0]
        var personagemEsperando = SKSpriteNode(texture: primeiroFrame) //Um sprite precisa ser iniciado com uma imagem, então pegamos a primeira do nosso array
        
        //Posicionando e adicionando o sprite na cena
        personagemEsperando.position = CGPoint(x: CGRectGetMidX(self.frame) + 200 , y:610)
        self.addChild(personagemEsperando)

        //Action que exibe todas as texturas em sequencia, de acordo com o tempo
        personagemEsperando.runAction(SKAction.repeatActionForever(
            SKAction.animateWithTextures(esperandoFrames,timePerFrame: velocidadeFrames))
        )
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            var touchNode = nodeAtPoint(location)
            
            if(btnVoltar .containsPoint(location)) {
                let cenaMenu = NavegacaoCenas(size:self.size)
                cenaMenu.scaleMode = scaleMode
                let reveal = SKTransition.doorwayWithDuration(1.5)
                self.view?.presentScene(cenaMenu, transition: reveal)
            }
        }
    }
}
