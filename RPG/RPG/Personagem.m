//
//  Personagem.m
//  RPG
//
//  Created by Mark Joselli on 3/4/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "Personagem.h"

@implementation Personagem

-(instancetype)initWithNome:(NSString*)nome{
    if(self = [super init]){
        
        self.nome = nome;
        
    }
    return self;
}

-(instancetype)initWithNome:(NSString *)nome Tipo:(NSString *)tipo Descricao:(NSString *)descricao ImagePath:(NSString *)imgPath{
    if(self = [self initWithNome:nome]){
        self.tipo = tipo;
        _descricao = descricao;
        _imgPath = imgPath;
    }
    return self;
}


@end
