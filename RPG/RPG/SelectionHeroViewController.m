//
//  SelectionHeroViewController.m
//  RPG
//
//  Created by Mark Joselli on 3/5/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "SelectionHeroViewController.h"

@interface SelectionHeroViewController ()

@end

@implementation SelectionHeroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self Popular];
    
    currentSelected = 0;
    self.LeftButton.hidden = YES;
    
    [self ChangeToHeroWithIndex:currentSelected];
    
}

-(void)ChangeToHeroWithIndex:(int)index{
    if(index < 0 || index > 4){
        NSLog(@"Erro personagem com indice %d não existe",index);
        return;
        
    }
    
    _NomeLabel.text = meusHerois[index].nome;
    _FotoImage.image = [UIImage imageNamed:meusHerois[index].imgPath];
    _ClasseLabel.text = meusHerois[index].tipo;
    _DescricaoText.text = meusHerois[index].descricao;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)Popular{
    meusHerois[0] = [[Personagem alloc]initWithNome:@"Micheli"];
    meusHerois[0].tipo = @"Ranger";
    meusHerois[0].descricao = @"Criativa";
    meusHerois[0].imgPath = @"micheli.png";
    
    meusHerois[1] = [[Personagem alloc]init];
    meusHerois[1].nome = @"Mark";
    meusHerois[1].tipo = @"Guerreiro";
    meusHerois[1].descricao = @"Conhecimento Técnico";
    meusHerois[1].imgPath = @"mark.png";
    
    meusHerois[2] = [[Personagem alloc]init];
    meusHerois[2].nome = @"Maicris";
    meusHerois[2].tipo = @"Druida";
    meusHerois[2].descricao = @"Coração";
    meusHerois[2].imgPath = @"maicris.png";
    
    meusHerois[3] = [[Personagem alloc]init];
    meusHerois[3].nome = @"Breno";
    meusHerois[3].tipo = @"Mago";
    meusHerois[3].descricao = @"Multiplas Atividades";
    meusHerois[3].imgPath = @"breno.png";
    
    meusHerois[4] = [[Personagem alloc]initWithNome:@"Binder" Tipo:@"Chefe" Descricao:@"Manda e desmanda!" ImagePath:@"binder.png"];
    
    
    NSLog(@"%@",meusHerois[4].nome);
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)LeftPressed:(id)sender {
    if(--currentSelected <= 0){
        currentSelected = 0;
        _LeftButton.hidden = YES;
    }
     _RightButton.hidden = NO;
    [self ChangeToHeroWithIndex:currentSelected];
}

- (IBAction)RightPressed:(id)sender {
    if(++currentSelected >= 4){
        currentSelected = 4;
        _RightButton.hidden = YES;
    }
    _LeftButton.hidden = NO;
    [self ChangeToHeroWithIndex:currentSelected];
}
@end
