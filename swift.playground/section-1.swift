// Playground - noun: a place where people can play



/*

Primeiramente vemos que temos um novo tipo de arquivo .playground. Esse tipo de arquivo nos permite testar código

*/

import UIKit



/*
vemos que o playground já começa com algumas coisas

primeiro com um import. nesse caso ele esta importando o framework UIKit, seria como se ele estivesse colocando codigos e funcionalidades já desenvolvidas, que podemos usar em nosso código.

Seria um paradigma parecido com vc colocar um USB em seu computador para poder usar os arquivos de dentro dele, só que neste caso estamos colocando código.


*/



var str = "Hello, playground"
var aux

aux = 3


/*

(para ver a diferenca de C e tal... vemos que não temos o main!!! )

depois estamos vendo que ele já cria um olá playground... isso vem começar a aprender a programar... que chamam de Olá, Mundo, normalmente esse é o primeiro código que uma pessoa vai desenvolver. A ideia delee é ter a funcionalidade basica para mostrar uma string (a tradução seria texto, mas já vou usar a palavra string que fica mais facil para nos acostumarmos aos paradigmas de programacao e da linguagem) 

Nesse caso estamos colocando (usando o comando =) em uma variavel (usando a palavra chave var) chamada str uma string com o conteudo "Hello, playground"

Podemos ver na esquerda que temos já o resultado do conteudo da var str.

no caso podemos para realmente termos um olá mundo, temos que usar uma funçao que realizará isto. 

para isso usamos a funçao println... no caso print vem de imprimir, e ln quer dizer para pular uma linha.
se vc colocar o icone no olho, vc pode ver o que está sendo imprimido no console.

*/
println("Hello, world")


var numeroInteiro = 10
var numeroFlutuante = 10.5

numeroInteiro = 20


/*
podemos ter outros tipos de variaveis como numeros inteiros e numeros com ponto flutuante.

Na verdade, numeroInteiro, numeroFlutuante e demais variaveis são na realidade posições de memoria. 

Então quando estamos fazendo 'numeroInteiro = 10' estamos colocando o valor 10 na posição de memoria 'numeroInteiro'. 

Se fizermos 'numeroInteiro = 20' estamos substituindo o valor que está armazenado em 'numeroInteiro'.

Vc pode ser perguntar, é tudo numero. Mas para o computador não... no mundo do computador, tudo são 0s e 1s... ou bits... que siginifica desligado e ligado... é a nossa unidade basica usadas nos computadores para armazenar e manipular dados.
agrupamos esses 0s e 1s em grupos de 8 e passamos a chamar de bytes. Com esses bytes criamos comando e signos para representar todo o mundo que vemos apresentado para nós nas nossas telas.
Então, para representar um numero inteiro, precisamos de bem pouca informação. Pois é só um numero... já para um numero de ponto flutuante, precisamos de muitos mais informação, pois depende da precisão que queremos atingir com o ponto flutuante (lembre-se das aulas da tia teteia... entre 0 e 1 temos infinitos numeros e infelizmente não temos infinitos bytes em nosso computador... mas o que tenmos normalmente é o suficiente). 

*/

var numero1 = 1,numero2 = 2

/*

também podemos ter mais de uma declaração de variavel nos utilizando da ',', como podemos ver no exemplo acima

*/





var meu🐴 = "é um ✈️"

/*
Uma das coisas que temos em swift... que acho que é unico na linguagem é o uso de Emojis... com o swift vc pode colocar emojis tanto em nome de vars quanto no conteudo das variaveis.
*/

var minhaString:String = "azul"
var meuInteiro:Int = 3
var meuPontoFlutuante:Float = 3.1
var meuPontoDouble:Double = 3.1


/*
O swift pega implicitamente o tipo da variavel quando colocamos o valor nela. Isto quer dizer que se colocarmos uma string, a var será do tipo string.
também podemos colocar explicitamente o tipo. Temos diversos tipos que podemos usar, como pode ser visto na tabela X.
 
dica: uma vez setada para um tipo, a variavel não pode ser mudada (ex meuInteiro="3" gerará um erro)
*/

var meuBoleano:Bool = true

/*
também temos o tipo booleano, que somente aceita dois valores true e false.
*/


numeroInteiro = 5

/*
 uma das caracteristicas das variaveis, é que seu conteudo pode variar, assim como o seu nome diz!!

TABELA COM TIPOS BASICOS

então no caso podemos mudar o valor de uma variavel de forma bem simpes, somente mudando o valor armazenado com o operador =. Como estamos mudando o valor do numeroInteiro de 3 para 5
*/

minhaString = "Meu numero \(numeroInteiro)"


meuPontoFlutuante = Float(numeroInteiro)

/*
para convertermos corretamente numeros inteiros devemos usar conversosres.
No caso podemos converter para String usando o String(valor a ser modificado) ou o \(valor a ser modificado).

Para os outros tipos temos somente o NomeDoTipo(ValorASerModificado)

*/


var meuOpcional:Int?

meuOpcional = 4

if let aux = meuOpcional{
    meuInteiro = aux
}

meuInteiro = meuOpcional!


var meuOpcionalExclama:Int!

meuOpcionalExclama = 3

meuInteiro = meuOpcionalExclama



let minhaConstante = 5

let minhaConstante2:String = meu🐴


/*
também temos constantes, que são regiões de memoria que somente podem receber um valor, e esse valor não pode ser modificado. Nesse caso usamos a palavra-chave let.
dica: tentar mudar o valor de constante causará um erro!
*/





var minhaSoma = minhaConstante + meuInteiro

var minhaSubtracao = minhaConstante - meuInteiro

var minhaMultiplicação = minhaConstante * meuInteiro

var minhaDivisão = minhaConstante / meuInteiro

var meuResto = minhaConstante % meuInteiro


/*

temos inicialmente o operador =.. que estamos usando até agora para colocar o valor.


operadores matemáticos são bem diretos. MAs eles devem ser usados entre variaveis do mesmo tipo. 

Podemos ver os tipos basicos na Tabela XX
*/
var minhaDivisãoComPonto = Float(minhaConstante) / Float(meuInteiro)

/*


Vemos que mesmo a divisão de minhaConstante por meuInteiro não dar 1... ele aproxima apra um. Caso quisessemos a parte depois do ponto teriamos que fazer um cast para o float ou double.

*/

9 % 4.3

/*
uma das novidade do swift, que grande parte das linguagens de programacao não tem... é resto de operações com ponto flutuante... no caso acima temos uma divisão de 9/4.3... que tem como resto 0.4
*/


5 + 2 * 4


(5 + 2) * 4
/*

Uma das coisas que temos é precedencia dos operadores. (Tabela com ordem de precedencia).

Caso na conta acima, quisesemos fazer 5+2 e depois multiplicar por 4... teriamos que usar os operadores () para colocar a precedencia da conta

*/



let minhaConstanteConcatenada:String = minhaString + meu🐴


/*
Também podemos usar o + para concatenar strings e criar novas strings.

Esses tipos de operadores são chamados de binarios, pois eles precisam de 2 argumentos. Um do lado esquerdo e outro do lado direito.

*/

meuInteiro == minhaConstante
meuInteiro != minhaConstante

meuInteiro > minhaConstante
meuInteiro < minhaConstante
meuInteiro >= minhaConstante
meuInteiro <= minhaConstante

/*

TEmos operadores de comparação, que comparam o valor de dois var.
Também temos o operador == que compara dois valores e verifica se eles são iguais (e retorna true caso isso aconteça)

E temos o != que verifica se esses valores são diferentes (e retorna true caso isso aconteça)


temos o maior que, menor que, maoir ou igual a, menor ou igual a


*/



meuInteiro++
++meuInteiro

--meuInteiro
meuInteiro--

meuBoleano = !meuBoleano

/*
também temos operadores unarios, que somente precisam de um argumento. No caso do ++ ele pega o valor e soma um a ele. então no caso o meuInteiro tinha valor 3 e foi para 4. Vemos que como colocamos esse valor depois da variavel, ele primeiro imprime esse valor e depois soma, caso queiramos somar e depois imprimir esse valor temos de colocar o ++ antes.

Tambem temos a mesma funcionalidade com o --, só que neste caso subtraimos 1 a o numero.

já o '!' o operador not... transforma o valor do boleano, então no caso, como ele tinha valor true... ele foi para valor falso. Ele é bastante importante para compararmos valores com o ==

*/

meuInteiro = -meuInteiro
meuInteiro = +meuInteiro
/*
tambem podemos usar o '-' como operador unario para modificar o sinal do valor de uma variavel

e o '+' que retorna sem modificar o valor
*/

meuInteiro <= minhaConstante ? 4 : 5
meuInteiro > minhaConstante ? 4 : 5

/*
temos também um tipo especial de operador o ?: que é um tipo condicional de operador. Ele tem o formato 'questão?resposta1:resposta2' Que no caso, caso a questão seja verdadeira, retorne a resposta1, caso contrario retorne a resposta2.
No exemplo, a pergunta sendo 'meuInteiro <= minhaConstante', como ela é verdadeira, ele retorna a resposta1, que no caso é 4

No segundo exemplo, a pergunta sendo 'meuInteiro > minhaConstante', como ela é falsa, ele retorna a resposta2, que no caso é 5



*/
meuInteiro == -3 || minhaConstante == -1
meuInteiro == -1 || minhaConstante == 5

meuInteiro == -3 && minhaConstante == -1
meuInteiro == -3 && minhaConstante == 5





/*

também temos operadores logicos, que Sào usados para combinar ou modificar valores de boolenos. No caso já vimos o '!', que muda o valor do booleano.

ou || é usado quando queremos comparar dois valores. No caso ele somente é false quando os algum dos valores são false. Caso um deles seja true, ou mesmo os dois... ele retorna como true. Um detalhe é que se o primeiro valor for true... ele já retorna true de uma vez e não continua os testes.

e && também é usado quando queremos comparar dois valores. No caso ele somente é true quando os algum dos valores são true. Caso um deles seja false, ou mesmo os dois... ele retorna como false. Um detalhe é que se o primeiro valor for false... ele já retorna false de uma vez e não continua os testes.


Para nos ajudar a sabermos o que será o resultado final, podemos usar Tabelas verdade, que são tabelas matematicas usadas em logica para matematica booleana.

TABELA VERDADE
*/

meuInteiro == -3 || minhaConstante == 5 && meuInteiro == -3 && minhaConstante == -1
meuInteiro == -3 || (minhaConstante == 5 && (meuInteiro == -3 && minhaConstante == -1))

(meuInteiro == -3 || minhaConstante == 5) && (meuInteiro == -3 && minhaConstante == -1)

/*
tambem podemos concatenar testes... efaze-los juntos... 

a precedencia é sempre da direita para esquerda... e usamos nomanente '(' e ')' para organizarmos isso

podemos ver no exemplo acima que as duas primeiras operações são equivalente... pois organizamos com o () as etapas que o compilador faz automaticamente... já a terceira operação é diferente... por organizamos de forma a realizar a segunda ioperação && por ultimo.

*/

var estáSol = true
if estáSol {
	println("vou a praia")
}

/*
Nossa ideia agora é usar controladores de fluxo para determinar o que deve ser feito de acordo com as nossas variaveis... 
Para isso introduziremos inicialmente o operado 'if' que que dizer se.

Para isso vamos dar um exemplo, digamos que somente iremos a praia se estiver sol. Para isso teremos uma variavel estáSol que é um booleano.

então "if estáSol" for true o programa executará o bloco de codigo que estiver dentro das chaves caso contrario e continuará a execução do programa.

*/

estáSol = false
if estáSol {
	println("vou à praia")
}else{
	println("vou ao cinema")
}

/*
E caso quizesse que se não estiver sol, eu irei ao cinema. Para isso temos o comando 'else' que nos caso o teste dentro do if seja falso, executa o codigo dentro do bloco depois do else. como mostra o codigo acima.

*/
var horas = 10
if estáSol && horas < 13{
	println("vou à praia")
}else if horas > 15{
	println("vou ao cinema")
}else{
	println("fico em casa")
}

/*
também podemos usar as operações boleanas para fazermos perguntas mais dificeis. Caso eu queira somente ir a praia se estiver sol e as horas forem antes das 13... pergunto 'estáSol && horas < 13'. E caso somente queira ir ao cinema se for mais de 15horas posso aninhar um if depois do else, fazendo 'else if (horas > 15)'. E podemos ainda ter mais um else (ou até mesmo varios, desde que aninhemos com ifs) para caso os dois testes anteriores tenham sido falsos.

*/
println("1")
println("2")
println("3")
println("4")
println("5")
println("6")
println("7")
println("8")
println("9")
println("10")


/*
uma das coisas boas da programação é que ele pode nos ajudar a fazer coisas repetivas. Digamos que gostariamos de contar de 1 até 10, impirmindo isso para o usuario. Dessa forma teriamos que fazer 10 linhas de codigo, como mostra acima.


*/
var aux = 0
for var i=1;i<=10;i++ {
	println(String(i))
}



/*
para isso poderismo usar laços, ou loops como é seu nome em ingles. Um loop é uma sequencia de código que pode é especificado uma vez, e pode ser repetido divesas vezes em sequencia. O Loop pode ser repetido por um certo numero de vezes, ou até que alguma condição ocorra.

Para isso podemos usar a palavra chave "for" que teria como tradução para. Esse tipo de loop seria controlado por um contador, no caso do codigo, a variavel i. A primeira parte (antes do primeiro ';') "var i=1" declara a variavel e coloca ela como valor de 1. Na segunda parte temos nossa condição de parada de execução "i<=10". E na terceira parte "i++" temos o incrementador, que incrementa a variavel para o proximo passo do loop. Entao o for funciona da seguinte maneira, antes de entrar no laço ele inicializa a variavel, e depois checa se a condição da segunda parte é satisfeita, se sim ele executa o código dentro das chaves '{}', e no final ele incrementa de acordo com a terceira parte. e repete ate que a condição da segunda parte nao seja mais satisfeita.

*/


var i = 1
while i <= 10{
	println(String(i));
	i++
}

i=1
do{
	println(String(i));
	i++
}while i <= 10

/*
poderiamos então substituir o código do 'for' por um codigo usando 'while' (que tem tradução de enquanto) que faz exatamento o que descrevemos anteriormente, só que em mais linhas.ou ate mesmo usando o do...while.
 
*/

i = 11
while i <= 10{
	println(String(i));
	i++
}

i=11
do{
	println(String(i));
	i++
}while i <= 10

/*A unica diferença do do...while para o while somente é qdo a condição é testada. No caso do while ele verifica a condiçaão e depois executa o bloco de códigos. Já no caso do do...while ele executa primeiro o codigo e depois verifica a condição. Como podemos ver no caso acima, se tivessemos o i com valor de '11'.'
*/


/*
i=1
do{
	println(String(i))
	i++
}while i >= 1


um dos cuidados que temos de ter qdo criarmos um loop, é verificar se ele não é infinito, isto é se ele nunca é parado. no caso acima inicializamos a variavel de controle i com 1 e incrementamos ela com 1... mas nossa condição de para é se ele for maior ou igual a 1... o que sempre será true!!! isso ocassionará o bloco a executar para sempre.
*/


i=1
do{
	println(String(i))
	i++
	if i==11{
		continue
	}
	if i>10{
		break
	}
}while i >= 1

/*
para se evitar isso podemos colocar uma condição dentro do laço, e usarmos a palavra-chave break, que no caso sai do loop.
Uma outra palavra chave que podemos usar no loop é o continue. que qdo chamado ignora o resto do codigo a ser executado e volta para o começo do código do loop.
*/


/*
A unica diferença do do...while para o while somente é qdo a condição é testada. No caso do while ele verifica a condiçaão e depois executa o bloco de códigos. Já no caso do do...while ele executa primeiro o codigo e depois verifica a condição. Como podemos ver no caso acima, se tivessemos o i com valor de '11'.'
*/




if horas == 1{
	
	println("1 hora")
}else if horas == 2{
	
	println("2 horas")
}else if horas == 3{
	
	println("3 horas")
}else if horas == 4{
	
	println("4 horas")
}

switch horas{
	case 1:
		println("1 hora")
	
	case 2:
		println("2 hora")
	
	case 3:
		println("3 hora")
	
	case 4,5:
		println("4/5 hora")
	
	default:
		println("hora nenhuma")
}

/*
o comando switch pega um valor e compara com diversas posibilidade. Dessa forma evitamos algumas linhas de codigo com if e else. As comparações são feitas com o case. também poderiamos comparar mais de uma vez no mesmo case, usando a ',' (que nem no codigo acima temos o case 4,5:). No switch temos tambem o comando default, que caso todas as comparações sejam falsa, é executado.

*/
func contarAte10(){
	
	for(var i=0;i<10;i++){
		println(i);
	}
	
}

contarAte10()
/*
temos funções que ajudam a fazer códigos repetitivos. Imagine que quisesemos ter um codigo que contasse de um até 10 e usassemos em diversos lugares. Uma opcao seria ter esse mesmo codigo copiado em varios lugares, outra opcao seria usar uma funçao. No caso usamos a palavra-chave func para criar uma funçao, seguido do nome que daremos a ela (no caso contarAte) e os parenteses.Depois segue-se da definição do codigo a usar, que no caso é um loop fazendo contando até o numero.

para chamar a funçao devemos apenas colocar o nome da func

*/

func contarAte(numero:Int){
	
	for(var i=0;i<numero;i++){
		println(i);
	}
	
}

contarAte(5)

contarAte(2)

/*
 MAs e esse numero muda, teriamos que implementar contarAte11, contarAte12... NAO! No caso usamos argumentos. Colocamos entre os parenteses a variavel com os argumentos que serão usados (no caso o numero:Int). Depois segue-se da definição do codigo a usar, que no caso é um loop fazendo contando até o numero.

para chamar a funçao devemos apenas colocar o nome da func seguida do argumento entre parentesees.
*/

func par(numero:Int) -> Bool{
	
	return Bool(numero%2);
}

par(2)
par(1)

/*
também temos como ter um retorno da função. Para isso usamos o "->" para indicar o tipo de retorno (que no caso é um booleano). E para fazer o retorno propriamente dito usamos a palavra chave "return"
*/

func maior(numero1 numero1:Int,  #numero2:Int) ->Int{
	if numero1 > numero2{
		return numero1
	}
	
	return numero2
	
}

maior(numero1: 1, numero2: 2)

/*
também podemos ter mais de uma argumento na função. é só separa-los por ',' :) e podemos dar nomes a eles, como no caso acima que chamamos os argumentos de numero1 e numero2.

Vemos que tivemos que escrever duas vezes o "numero1". Se quisermos que o nome do argumento e a variavel tenham o mesmo nome, podemos eliminar a segunda vez que colocamos o nome usando o "#", como fizemos com o numero2 (e até mesmo o compilador te dar um warning sobre o nume1 para vc fazer o mesmo).
*/


func retorna2emStringeInt() ->(String,Int){
	var string = "2";
	var numero = 2
	
	return (string,numero)
	
}

let dois = retorna2emStringeInt()

2 + dois.1



/*
uma dos diferenciasis do swift é que vc pode agrupar multiplos valores em um unico valor composto, que chamamos de tuple. No caso de nossa função temos dois valores, um string e um int que são retornados pela função.

para usar os valores temos que pega-los pela ordem q eles aparecem, usando um numeral. então no caso para acessar o inteiro temos de faser dois.1...

poderiamos também facilitar nossa vida e usar nomes para pegar os valors mudando a função para "func retorna2emStringeInt() ->(string:String,inteiro:Int)". Com isso para pegar o valor inteiro podemos usar dois.inteiro

*/

var meuTuple = (numero:1,string:"string",floatNumber:1.2)


/*
também podemos definir tuples como variavaeis. No caso estou definindo a variave meuTuple para ter um tuple com 3 valores, um int, uma string e um float

*/



func junta(#string1:String,string2:String = "!") -> String {
	
	return string1 + string2;
	
}



junta(string1: "as")


/*
Mas voltanda as funções...
podemos também colocar arumentos padrões, que caso o desenvolvedor não quiser prover o argumento, ele manda o padrão. No caso ele funciona da mesma forma que guardamos um valor em uma var (= valor). No exemplo temos a função junta, que tem o segundo argumento como string que tem valor padrão de "!".
*/


var juntaString = junta

juntaString(string1: "b",string2: "c")

/*
também podemos guardar uma func em uma variavel. no caso do exemplo pode parecer que não tem muita funcionalidade, pois só mudou o nome, mas dessa forma podemos passar funções como parametros de outras funções.

*/

func Soma2(inout num1:Int){
	num1 = num1 + 2
}


/*
quando passamos um argumento, ele no caso é uma copia dele e é uma constante, então não podemos modificar o seu valor. Para podemos fazer isso temos de usar a palavra chave "inout" que fala que a variavel pode ser modificada dentro da func.
*/


var minhaLista:[Int] = [1,2,4,4]

var minhaSegundaLista = [1,"2",3,4]


/*
arrays são listas, onde vc pode colocar mais de uma valor na mesma variavel. Os valores podem ser do mesmo tipo (minhaLista), ou de tipos diferentes (minhaSegundaLista)
 

*/

minhaLista[2]

minhaLista[2] = 3

/*
para acessar os valores das variaveis, somente colocamos o indice deles dentro do []. No caso do exemplo colocando o indice 2 (minhaLista[2]) estamos pegando o terceiro valor (o 0 é o primeiro), que no caso é 3. E podemos da mesma forma modificarmos seu valor.
*/

minhaLista.count
minhaLista.isEmpty


/*
tambem temos como saber quantos elementos tem dentro do array usando a propriedade "count". Tam'bem temos a propriedade isEmpty que verifica se  o array es'ta vazio
*/


minhaLista.append(5)


minhaLista.insert(0,atIndex:0)

/*
podemos adiconar itens no final do array chamando o append e passando o valor como parametro

ou podemos definir onde iremos inserir o novo valor com o insert, passando o valor e o indice como parametros.
*/

minhaLista[0...3] = [-1,-2,-3]

/*
podemos também acessar/modificar diversos valores do array de uma só vez. Como no exemplo, modificamos os valores que estão nos indices 0,1,2 para -1,-2,-3 usando o operador "..."
*/

minhaLista.removeLast()
minhaLista.removeAtIndex(2)

/*
também podemos remover itens. Podemos remover o ultimo item usando removeLast, ou remover um item em um indice especifico. No exemplo estamos removendo o item no indice 2, ai no caso, o indice 2 é preenchido com o valor que estava no indice 3, o indice 3 é preenchido com o valor que estava no indice 4, e assim por diante até o ultimo item
*/

for item in minhaLista{
	println("item\(item)")
}

/*
também podemos interagir dentro do array usando o for. No caso usamos uma variavel auxiliar e a palavra-chave 'in' para indicar a coleção que vamos usar e depois a coleção.


*/
var meusDados = ["nome":"Mark","Sobrenome":"Joselli"]

var meusDados2 = [String:String]()
/*
O dicionario tem o mesmo principio que o Array, mas ao inves de usarmos indices numericos, usamos uma chave, que pode ser qualquer tipo de variavel. No exemplo acima temos duas chaves "nome" e "sobrenome", para guardar os respectivos valores "Mark" e "Joselli".
Também podemos criar um dicionario vazio, mas no caso já devemos definir o tipo das chaves e dos valores.

*/

meusDados.count

/*
também podemos saber o tamanho do dictionary com o "count"

*/

meusDados["nome"]

meusDados["nome"] = "José"

meusDados.updateValue("Carlos", forKey: "Sobrenome")

/*
para acessar e modificar é só passar a chave dentro dos []. Ou podemos chamar o metodo updateValue, passando o valor e a chave como parametros.

*/

meusDados["Cidade"] = "Curitiba"

/*
para adicionar um novo item seria somente colocar uma nova chave dentro dos [] e passar o valor que ela terá, como fazemos acima introduzindo o valor Curitiba na chave cidade
*/


meusDados["nome"] = nil
meusDados.removeValueForKey("Sobrenome")

/*
para remover temos duas opções: ou acessamos a chave que queremos, com o [], e colocamos o valor de nil para ele; ou usamos o metodo removeValueForKey passando como parametro a chave que queremos remover.
*/


for(chave,valor) in meusDados{
	println("meusDados[\(chave)] = \(valor)")
}

for chave in meusDados.keys{
	println("meusDados[\(chave)]")
}

for valor in meusDados.values{
	println(" = \(valor)")
}

/*
para iterar com o dictionary, fazemos o mesmo que no Array, so que no caso temos duas variaves, uma para a chave e outra para o valor.
*/

for chave in meusDados.keys{
	println("meusDados[\(chave)]")
}

for valor in meusDados.values{
	println(" = \(valor)")
}

/*
também podem interagir somente com as chaves, usando o "keys" ou somente com os valores, usando o "values". No casos há oretorno de um array e interagimos da mesma forma que com um array

*/

struct CarroStruct {
	var rodas = 3
	var portas = 3

	func acelerar(){

		println("vrum vrum");
	}
	
}

var meuCarroStruct = CarroStruct()


meuCarroStruct.rodas = 4

meuCarroStruct.acelerar()

meuCarroStruct = CarroStruct(rodas: 4, portas: 5)


/* 

Podemos criar structs que são estruturas usadas para criar novas variaveis. No caso ele define uma estrutura onde podemos ter propriedades e func dentro dele

podemos já criar um struct fazendo a inicialização das variaveis dele.
*/

class CarroClass {
	var rodas = 3
	var portas = 3

	func acelerar(){

		println("vrum vrum");
	}
	
}



class Porta{
	private var cor = "azul"
	private var estado = 0 // 0-fechado; 1- aberto
	
	func abrirPorta() { estado = 1}	func fecharPorta(){estado = 0}	func setarCor(){}	func verificarCor(){}	init(){}//construtor
	
	
}

var objetoPorta = Porta()

objetoPorta.abrirPorta()


let meuCarroClass = CarroClass()


meuCarroClass.rodas = 4

meuCarroClass.acelerar()




/*
e tb temos classes!

Mas um minuto! eles não sao iguais!

sim... no exemplo eles são iguais, mas a classe tem features a mais que a struct.

Qdo vc cria uma classe ou struct, vc na verdade esta criando um novo tipo que pode ser usado. Mas temos diferenças.

Video explicando orientação a objetos.

*/

var meuCarroStructCopy = meuCarroStruct
meuCarroStructCopy.portas = 4
meuCarroStruct.portas


var meuCarroClassReferencia = meuCarroClass
meuCarroClassReferencia.portas = 4
meuCarroClass.portas


/*
no caso, os structs são copiados qdo vc faz o '='. entao se vc muda o valor de alguma propriedade da copia, ele so muda na copia, o original fica inalterado.

e a classe não, ela é somente referenciada. Nesse caso se vc mudar o valor de alguma propriedade na referencia, muda também no original.

*/

if(meuCarroClass === meuCarroClassReferencia){
	println("iguais")
	
}
if(meuCarroClass !== meuCarroClassReferencia){
	println("diferentes")
}

/*
 temos um operador para verificar se dois objetos referenciam a mesma posição de memoria, que é 0 "===". e temos um operador para verificar se eles são diferente que é o "!=="

*/

class Veiculo {
    var rodas:Int
	
	init(rodas:Int){
	self.rodas = rodas
	}
	
	func descrição() -> String{
		return "tenho \(rodas) rodas"
	}
	
}

var meuVeiculo = Veiculo(rodas: 2)



/*
temos na classe uma função especial, que é chamada sempre que a classe é criada, o nome dessa func é "init". então qualquer valor que gostariamos de inicializa, ou fazer algo qdo o objeto é criado deve ser feito nessa func. Uma das coisas possiveis é passarmos valores nessa func. No nosso exemplos temos o unit que passamos uma variavel do tipo int, que tem nome rodas. Um dos problemas que podemos ter, e que temos tam'bem uma variavel rodas na classe. Nesse caso o codigo precisa de algo para diferenciar as variaveis. No caso o rodas sem nada seria a variavel local, a variavel que vem como argumento, e para acessarmos a variavel da classe, temos que usar o comando "self" que é o ponteiro da propria classe. Com isso podemos colocar no valor rodas da classe.

também podemos usar a func deinit para limpar memoria qdo o objeto for destruido.

*/


class Moto :Veiculo{
	
	init(){
		super.init(rodas:2)
	}
	
	override func descrição() -> String {
		return "sou uma moto e \(super.descrição())"
	}
	
}

var minhaMoto = Moto()
minhaMoto.descrição()


/*
uma das coisas interessante de objetos é a herança. Isto é, temos cmo herdar uma classe. Ai no caso, pegamos todos as propriedades e metodos já deifinidos na classe pai.
No exemplo acima, podemo ter uma classe moto, que herda da classe veiculo (usando o :Veiculo). E no caso, no init dele, ele já inicializa com rodas=2 , chamando o init da classe pai (com o comando "super").
Outra funcionalidade é que vc pode reescrever funcs do pai, usando o comando "overide", como fazemos no exemplo com o descrição, colocando sou uma moto nates da descrição da classe pai.

*/

class Quadrado{
	var largura:Float
	
	init(largura:Float){
		self.largura = largura
	}
	
	var area:Float{
		set{
			largura = sqrtf(newValue)
			
		}
		get{
			return largura*largura
		}
		
	}
	
}

var meuQuadrado = Quadrado(largura: 3.0)

meuQuadrado.area

meuQuadrado.area = 16

meuQuadrado.largura


/*
quando colocamos um valor em uma propriedade da classe, dizemos que estamos fazendo o set dela, e qdo pegamos esse valor dizemos que estamos fazendo o get. Podemos substitiur esses set e get para func customizadas, fazendo com que temos uma variavel auxiliar area, que já calcula automaticamente o area a partir da largura no get, e calcula a largura a partir da area no set.

*/


class ClasseCustomizada{
	var meuInt:Int{
		willSet{
			println("vai mudar do valor \(meuInt) para \(newValue)")
			
		}
		didSet{
			println("mudou o valor para \(meuInt)")
		}
	}
	
	init(){
		self.meuInt = 3
	}
}

var minhaClasseCustomizada = ClasseCustomizada()

minhaClasseCustomizada.meuInt = 5
/*

também podemos ter funcs auxiliares dentro da variavel apra verificar qdo ela vai mudar de valor (willSet) e qdo ela teve o valor mudado (didSet). No exemplo acima ficamos verificamos essas funcs e imprimimos no console qdo isso ocorre.
*/


var minhaClasse:ClasseCustomizada?

minhaClasse = minhaClasseCustomizada



/*
tinhamos dito que toda variavel tem de ser inicializada. Bom mentimos!
 A principio toda variavel deve ser inicializada. mas ela pode ser um opcional, com o uso do '?'. neste caso a variavel tem valor nil. Nesse caso é obrigatorio vc colocar o tipo de variavel.
O ? diz: existe um valor e esse valor é tal, ou não existe valor nenhum.

*/

println(minhaClasse!.meuInt)


/*
tam'bem temos o operador !, que é usado com opcionais. Esse operador força dizermos que existe realmente um valor associado a variavel. veja no exemplo acima que se tirarmos o ! dá um erro de compilação!


*/

if let minhaClasseOptional = minhaClasse{
	println(minhaClasseOptional.meuInt)
}

/*
caso não temos certeza se a variavel foi inicializada ou não, temos de usar um if e uma variavel auxiliar para verificar se ela foi ou não inicializada. No caso acima, usamos a constante minhaClasseOptional para guardar o valor, somente se ele existe, caso contrario ele não entra dentro do if. e somente depois imprimimos o resultado.

*/

minhaClasse = nil

/*
com o opcionais podemos colocar eles de novo com valor nenjum, como fazemos no exemplo acima.
*/
























